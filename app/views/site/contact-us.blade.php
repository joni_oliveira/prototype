@extends('site/layouts/master')

@section('css')
{{Html::style('assets/css/contact.css')}}
@stop

@section('container')
	<div class="container_12">
		<div class="grid_12">

			<ul id="distribuitors">
				<li>
					{{Html::image('assets/img/pt_ico.png', 'pt_logo')}}
					<h2>Distribuído por <span>RacingCycle</<span></h2>
					{{Html::mailto('bm@racingcycle.net','bm@racingcycle.net')}}
				</li>
				<li>
					{{Html::image('assets/img/es_ico.png', 'pt_logo')}}
					<h2>Distribuido por <span>Oriol Morata</<span></h2>
					{{Html::mailto('oriol@oriolmorata.com','oriol@oriolmorata.com')}}
				</li>
				<li>
					<h2 id="outro_pais">Other Country</h2>
					{{Html::mailto('sales@prototype.pt','sales@prototype.pt')}}
				</li>
			</ul>			
		</div>
	</div>
@stop