@extends('site/layouts/master')

@section('css')
{{Html::style('assets/js/nivo-slider/nivo-slider.css')}}
{{Html::style('assets/js/nivo-slider/themes/bar/bar.css')}}
{{Html::style('assets/js/capty/jquery.capty.css')}}
@stop

@section('script')
{{Html::script('assets/js/nivo-slider/jquery.nivo.slider.pack.js')}}
{{Html::script('assets/js/capty/jquery.capty.min.js')}}
@stop

@section('container')
	    <div class="container_12">
                <div class="slider-wrapper grid_12 omega theme-bar">
                    <div id="slider" class="nivoSlider">
                        <img src="assets/img/slides/banner_prototype_xx1.jpg" alt="" />
                        <img src="assets/img/slides/banner_prototype_physis.jpg" alt="" />
                        <img src="assets/img/slides/banner_prototype_gxp.jpg" alt="" />
                        <img src="assets/img/slides/banner_prototype_ch3.jpg" alt="" />
                    </div>
                </div>
                <div id="news" class="grid_12">
                    <div id="lst-news"class="grid_6 alpha">
                        <h2>Última Notícia</h2>
                        {{Html::image('assets/img/news/'.$post->img, $post->title, array('id'=> 'lst-news-img','name'=> '#content-news-target'))}}
                        <!-- <img id="lst-news-img" name="#content-news-target" src="assets/img/news/ivo-santos.png" alt="Ivo Santos"> -->
                        <div id="content-news-target">
                            <p>{{$post->title}}</p>
                            <a href="{{$post->url()}}" target="_blank">Ler Mais</a>
                        </div>
                    </div>
                    <div id="lst-product" class="grid_6 omega">
                        <h2>Último Produto</h2>
                                 {{Html::image('assets/img/products/350/'.$product->getMainImage()[0]->path, $product->name ,array('id'=>'lst-product-img', 'name'=>'#content-product-target'))}}
                                <!-- <img id="lst-product-img" name="#content-product-target" src="assets/img/news/jockey-wheel.png" alt="Jockey Wheel"> -->
                            <div id="content-product-target">
                                <p>Novo {{$product->name}}</p>
                                <a href="{{$product->url()}}" target="_blank">Página do Produto</a>
                            </div>
                        
                    </div>
                </div>
        </div>
<script type="text/javascript">
$(window).load(function() {
    $('#slider').nivoSlider({
        effect: 'random', // Specify sets like: 'fold,fade,sliceDown'
        slices: 15, // For slice animations
        boxCols: 8, // For box animations
        boxRows: 4, // For box animations
        animSpeed: 500, // Slide transition speed
        pauseTime: 3000, // How long each slide will show
        startSlide: 0, // Set starting Slide (0 index)
        directionNav: true, // Next & Prev navigation
        controlNav: true, // 1,2,3... navigation
        controlNavThumbs: false, // Use thumbnails for Control Nav
        pauseOnHover: true, // Stop animation while hovering
        manualAdvance: false, // Force manual transitions
        prevText: '<<', // Prev directionNav text
        nextText: '>>', // Next directionNav text
        randomStart: true, // Start on a random slide
     });
    $('#lst-news-img').capty({
        cWrapper: 'captyWapperLeft',
    });
    $('#lst-product-img').capty({
        cWrapper: 'captyWapperRight',
    });
});
</script>
@stop