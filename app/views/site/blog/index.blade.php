@extends('site/layouts/master')

@section('css')
{{Html::style('assets/css/posts.css')}}
@stop

{{-- Content --}}
@section('container')
<div class="container_12">
<?php $i=0;?>
@foreach ($posts as $post)

	<div class="grid_12 post">
		<!-- Post Title -->
		<div class="grid_12">
			<!-- <h4><a href="{{{ $post->url() }}}">{{ String::title($post->title) }}</a></h4> -->
			<h4>{{ $post->title}}</h4>
		</div>
		<div class="clear"></div>
		<!-- ./ post title -->

		<!-- Post Content -->
		<div class="grid_4 alpha post-image">
			<a href="{{{ $post->url() }}}" class="thumbnail">{{Html::image('assets/img/news/'.$post->img)}}</a>
		</div>
		<div class="grid_8 omega post-content">
			<span class="post-date">{{String::date($post->created_at)}}</span>
			<p>
				{{ String::tidy(Str::limit($post->content, 600)) }}
				<!-- {{String::tidy($post->content)}} -->
			</p>
			<p><a class="btn-read-more" href="{{{ $post->url() }}}">Ler Mais</a></p>
		</div>
		<!-- ./ post content -->
	</div>
<?php $i++;?>
@if(count($posts)!=$i)
<hr />
@endif
@endforeach
</div>

@stop
