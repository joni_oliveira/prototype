<!DOCTYPE Html>
<!--[if lt IE 7]>      <Html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <Html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <Html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <Html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="google-site-verification" content="D5FQKBERcDDIdUjrX1Cb-zQycyBH7oaz5IaUySCW9Ww" />
        <title>Prototype Racing Bike Parts</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        {{Html::style('assets/css/normalize.min.css')}}
        {{Html::style('assets/css/960.css')}}
        {{Html::style('assets/css/text.css')}}
        {{Html::style('assets/css/main.css')}}
        @yield('css')


        {{Html::script('assets/js/modernizr-2.6.2-respond-1.1.0.min.js')}}
        {{Html::script('assets/js/jquery-1.9.1.min.js')}}
        <script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-36875952-1']);
			_gaq.push(['_trackPageview']);

  			(function() {
		    	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  			})();
		</script>
        @yield('script')
    </head>
    <body>
       <header>
            <a href="{{URL::Action('BaseController@getIndex')}}"><div id="logo"></div></a>
            <nav id="top-nav">
                <!--<ul id="language">
                    <li><a href="#">PT</a></li>
                    <li><a href="#">EN</a></li>
                    <li><a href="#">ES</a></li>
                </ul-->
				<ul id="social">
                    <li class="social-link"><a href="#"><img src="{{asset('assets/img/social-icons/facebook.png')}}" alt=facebook/></a></li>
                </ul>  
                {{HTML::linkAction('RegisterController@Index', 'Registar Produto', null, array('class'=>'register-link'))}}
            </nav>
            <nav id="main-nav">
                <ul>
                    <li>
                        <a href="#">Produtos</a>
                        <ul>
                            <li><a href="#">Rodas</a>
                                <ul>
                                     <li><a href="#">Estrada</a>
                                        <ul>
											<li>{{Html::linkAction('ProductController@getProduct','PRO TOUR 3 Clincher', array("productSlug"=>'3-pro-tour-c'))}}</li>
											<li>{{Html::linkAction('ProductController@getProduct','PRO TOUR 3 Tubular', array("productSlug"=>'3-pro-tour-t'))}}</li>
											<li>{{Html::linkAction('ProductController@getProduct','PRO TOUR 5 Clincher', array("productSlug"=>'5-pro-tour-c'))}}</li>
											<li>{{Html::linkAction('ProductController@getProduct','PRO TOUR 5 Tubular', array("productSlug"=>'5-pro-tour-t'))}}</li>
											<li>{{Html::linkAction('ProductController@getProduct','PRO TOUR 2 SL', array("productSlug"=>'2-pro-tour-sl'))}}</li>
											<li>{{Html::linkAction('ProductController@getProduct','PRO TOUR 3 SL', array("productSlug"=>'3-pro-tour-sl'))}}</li>
											<li>{{Html::linkAction('ProductController@getProduct','PRO TOUR 5 SL', array("productSlug"=>'5-pro-tour-sl'))}}</li>
                                        </ul>
                                    </li> 
                                    <li><a href="#">Montanha</a>
                                        <ul>
                                            <li>{{Html::linkAction('ProductController@getProduct','Race', array("productSlug"=>'wheel-mtb-race'))}}</li>
                                            <li>{{Html::linkAction('ProductController@getProduct','26R Carbon', array("productSlug"=>'wheel-mtb-26-carbon'))}}</li>
                                            <li>{{Html::linkAction('ProductController@getProduct','650B', array("productSlug"=>'wheel-mtb-650b'))}}</li>
                                            <li>{{Html::linkAction('ProductController@getProduct','650B Carbon', array("productSlug"=>'wheel-mtb-650b-carbon'))}}</li>
                                            <li>{{Html::linkAction('ProductController@getProduct','29R', array("productSlug"=>'wheel-mtb-29'))}}</li>
                                            <li>{{Html::linkAction('ProductController@getProduct','29R Carbon', array("productSlug"=>'wheel-mtb-29-carbon'))}}</li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
							<li><a href="#">Acessorios Tubeless</a>
								<ul>
									<li>{{Html::linkAction('ProductController@getProduct','Valvula Bronze', array("productSlug"=>'bronze-valve'))}}</li>
									<li>{{Html::linkAction('ProductController@getProduct','Valvula Aluminio', array("productSlug"=>'aluminium-valve'))}}</li>
									<li>{{Html::linkAction('ProductController@getProduct','Fita UST', array("productSlug"=>'ust-tape'))}}</li>
<li>{{Html::linkAction('ProductController@getProduct','CH3', array("productSlug"=>'ch3'))}}</li>
								</ul>
							</li>
							<li><a href="#">Anilhas</a>
								<ul>
									<li>{{Html::linkAction('ProductController@getProduct','Direção', array("productSlug"=>'spacer-headset'))}}</li>
									<li>{{Html::linkAction('ProductController@getProduct','Mov. Pedaleiro', array("productSlug"=>'spacer-bottombracket'))}}</li>
									<li>{{Html::linkAction('ProductController@getProduct','Prato Ped.', array("productSlug"=>'spacer-crank'))}}</li>
								</ul>
							</li>
							<li>{{Html::linkAction('ProductController@getProduct','Aperto Cassete', array("productSlug"=>'lockring'))}}</li>
							<li>{{Html::linkAction('ProductController@getProduct','Aperto Espigão', array("productSlug"=>'seat-clamp'))}}</li>
							<li>{{Html::linkAction('ProductController@getProduct','Avanço', array("productSlug"=>'physis-stem'))}}</li>
							<li>{{Html::linkAction('ProductController@getProduct','Caixa Carbono', array("productSlug"=>'carbon-cage'))}}</li>
							<li><a href="#">Cubos</a>
								<ul>
									<li><a href="#">Montanha</a>
										<ul>
											<li>{{Html::linkAction('ProductController@getProduct','Evo 3 Frente', array("productSlug"=>'evo3-fw'))}}</li>
											<li>{{Html::linkAction('ProductController@getProduct','Evo 3 Trás', array("productSlug"=>'evo3-bw'))}}</li>
											<li>{{Html::linkAction('ProductController@getProduct','Evo 3 Lefty', array("productSlug"=>'evo3-lefty'))}}</li>
										</ul>
									</li>
									<li><a href="#">Estrada</a>
										<ul>
											<li>{{Html::linkAction('ProductController@getProduct','Speed Frente', array("productSlug"=>'speed-fw'))}}</li>
											<li>{{Html::linkAction('ProductController@getProduct','Speed Trás', array("productSlug"=>'speed-bw'))}}</li>
										</ul>
									</li>
								</ul>
							</li>
							<li>{{Html::linkAction('ProductController@getProduct','Espigão de Selim', array("productSlug"=>'physis-seatpost'))}}</li>
							<li>{{Html::linkAction('ProductController@getProduct','Extensor Guiador', array("productSlug"=>'physis-barend'))}}</li>
							<li><a href="#">Merchadising</a>
								<ul>
									<li>{{Html::linkAction('ProductController@getProduct','Bidão', array("productSlug"=>'bottle'))}}</li>>
								</ul>
							</li>
							<li>{{Html::linkAction('ProductController@getProduct','Guiador', array("productSlug"=>'physis-handlebar'))}}</li>
							<li><a href="#">Mov. Pedaleiro</a>
								<ul>
									<li>{{Html::linkAction('ProductController@getProduct','BSA', array("productSlug"=>'bb-bsa'))}}</li>
									<li>{{Html::linkAction('ProductController@getProduct','GXP', array("productSlug"=>'bb-gxp'))}}</li>
								</ul>
							</li>
							<li><a href="#">Parafusos AL</a>
								<ul>
									<li>{{Html::linkAction('ProductController@getProduct','M5', array("productSlug"=>'bolt-m5'))}}</li>
									<li>{{Html::linkAction('ProductController@getProduct','M6', array("productSlug"=>'bolt-m6'))}}</li>
									<li>{{Html::linkAction('ProductController@getProduct','Kit Pedaleiro', array("productSlug"=>'bolt-crack-kit'))}}</li>
									<li>{{Html::linkAction('ProductController@getProduct','Disco', array("productSlug"=>'disc-bolt'))}}</li>
								</ul>
							</li>
							<li><a href="#">Rolamentos</a>
								<ul>
									<li>{{Html::linkAction('ProductController@getProduct','Black Shield', array("productSlug"=>'bearing-black-shield'))}}</li>
								</ul>
							</li>
							<li>{{Html::linkAction('ProductController@getProduct','Roldanas', array("productSlug"=>'jockey-wheels'))}}</li>
                            <!-- <li><a href="#">Aros</a>
                                <ul>
                                     <li><a href="#">Road</a>
                                        <ul>
                                            <li><a href="#">PRO TOUR</a></li>
                                            <li><a href="#">PRO TOUR 3 Clincher</a></li>
                                            <li><a href="#">PRO TOUR 3 Tubular</a></li>
                                            <li><a href="#">PRO TOUR 5 Clincher</a></li>
                                            <li><a href="#">PRO TOUR 5 Tubular</a></li>
                                            <li><a href="#">PRO TOUR 2SL</a></li>
                                            <li><a href="#">PRO TOUR 3SL</a></li>
                                            <li><a href="#">PRO TOUR 5SL</a></li>
                                        </ul>
                                    </li> 
                                    <li><a href="#">Montanha</a>
                                        <ul>
                                            <li>{{Html::linkAction('ProductController@getProduct','Race', array("productSlug"=>'rim-mtb-26'))}}</li>
                                            <li>{{Html::linkAction('ProductController@getProduct','26R Carbon', array("productSlug"=>'rim-mtb-26-carbon'))}}</li>
                                            <li>{{Html::linkAction('ProductController@getProduct','650b', array("productSlug"=>'rim-mtb-650b'))}}</li>
                                            <li>{{Html::linkAction('ProductController@getProduct','650b Carbon', array("productSlug"=>'rim-mtb-650b-carbon'))}}</li>
                                            <li>{{Html::linkAction('ProductController@getProduct','29R', array("productSlug"=>'rim-mtb-29'))}}</li>
                                            <li>{{Html::linkAction('ProductController@getProduct','29R Carbon', array("productSlug"=>'rim-mtb-29r-carbon'))}}</li>
                                        </ul>
                                    </li>
                                </ul>
                            </li> 
                            <li><a href="#">Acessorios Tubeless</a>
                                <ul>
                                     <li><a href="#">Road</a>
                                        <ul>
                                            <li><a href="#">PRO TOUR</a></li>
                                        </ul>
                                    </li> 
                                    <li><a href="#">Montanha</a>
                                        <ul>
                                            <li>{{Html::linkAction('ProductController@getProduct','Race', array("productSlug"=>'rim-mtb-26'))}}</li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>-->
                        </ul>
                    </li>
                    <li><a href="#">Especificações</a>
                        <ul>
                            <li>{{Html::linkAction('SpecController@getTech', 'Tecnologias')}}</li>
                            <li>{{Html::linkAction('SpecController@getOptions', 'Opções')}}</li>
                            <li>{{Html::linkAction('SpecController@getFeatures', 'Caracteristicas')}}</li>
                        </ul>
                    </li>
                    <li>{{Html::linkAction('SupportController@getIndex', 'Suporte')}}</li>
                    <li>{{Html::linkAction('BlogController@getIndex', 'Noticias')}}</li>
                    <li><a href="#">Eventos</a></li>
                    <li>{{Html::linkRoute('contact-us','Contactos')}}"</li>
                </ul>
            </nav>
       </header>
       <div id="container">
            @yield('container')
       </div>

       <footer>
           <div class="container_12">
               <div class="grid_6" id="design">
                   <p>Design by <img src="{{asset('assets/img/racingdesign.png')}}" alt="Racing Design"></p>
               </div>
               <div class="grid_6" id="copyright">
                   <p>Prototype &#8482;</p>
               </div>
           </div>
       </footer>

        <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>

        <script src="js/main.js"></script>
		-->
    </body>
</Html>