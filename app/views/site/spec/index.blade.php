@extends('site/layouts/master')

@section('css')
{{Html::style('assets/css/spec.css')}}
@stop

{{-- Content --}}
@section('container')
<div class="container_12">
<?php $i=0;?>
@foreach ($specs as $spec)
	<div class="grid_12 spec">
		<!-- Tech About -->
		<div class="grid_2 alpha">
			{{Html::image('assets/img/spec/'.$spec->img)}}
		</div>
		<div class="grid_10 omega spec-content">
			<p>
				{{String::tidy($spec->content())}}
			</p>
		</div>
		<!-- ./ spec about -->
	</div>
<?php $i++;?>
@if(count($specs)!=$i)
<hr />
@endif	
@endforeach
</div>

@stop
