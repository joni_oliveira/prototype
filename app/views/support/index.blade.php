@extends('site/layouts/master')

@section('css')
{{Html::style('assets/css/support.css')}}
@stop

{{-- Content --}}
@section('container')
<div class="container_12">
	@foreach($categories as $categorie)
		<div class="grid_12">
			<h2>{{$categorie->name}}</h2>
			@foreach($categorie->supports as $support)
				{{HTML::link(URL::to('assets/files/support/'.$support->path), $support->name, array('target'=>"_blank", 'class'=>"support-link"))}}
			@endforeach
		</div>
	@endforeach
</div>

@stop
