@extends('site/layouts/master')

@section('css')
{{Html::style('assets/css//smoothness/jquery-ui-1.10.3.custom.css')}}
{{Html::style('assets/css/products.css')}}
{{Html::style('assets/css/magnific-popup.css')}}
@stop

@section('script')
{{Html::script('assets/js/jquery-ui-1.10.3.custom.js')}}
{{Html::script('assets/js/jquery.magnific-popup.js')}}
@stop

@section('container')
	<div class="container_12">
		<div class="grid_12">
			<div class="grid_6 alpha">
				<div id="product_main_image">
					<a class="product_main_image_link" href="{{asset('assets/img/products/1000/'.$mainimage[0]->path)}}">{{Html::image('assets/img/products/350/'.$mainimage[0]->path)}}</a>
				</div>
				<div id="other_images">
					@foreach($otherimages as $i)
						<div class="thumb">
							<a class="product_main_image_link" href="{{asset('assets/img/products/1000/'.$i->path)}}">{{Html::image('assets/img/products/100/'.$i->path)}}</a>
						</div>
					@endforeach
				</div>
			</div>
			<div class="grid_6 omega">
				<div id="product_logo">
					{{Html::image('assets/img/products/logo/'.$logo[0]->path)}}
				</div>
				<div id="spec">
					@foreach($mainspecs as $mainspec)
						{{Html::image('assets/img/spec/'.$mainspec->img)}}
					@endforeach
				</div>
				<div id="tabs">
					<ul>
						<li ><a href="#tabs-1">Apresentação</a></li>
						<li ><a href="#tabs-2">Especificações</a></li>
						<li ><a href="#tabs-3">Saiba +</a></li>
						<li ><a href="#tabs-4">Manual</a></li>
					</ul>
						<div id="tabs-1">
								{{$description}}
						</div>
						<div id="tabs-2">
							<table>
									<?php $i = 0; ?> 
									@foreach($features as $feature)
									
										@if($i%2==0)
											<tr class="even">
										@else
											<tr class="odd">
										@endif
											<td class="feature-name">{{$feature->name}}</td>
											<td class="feature-description">{{$feature->description}}</td>
										</tr>
									<?php $i++; ?>
									@endforeach
								</table>		
						</div>
						<div id="tabs-3">
							<div class="spec-tech">
								@if(count($specsTech)>0)
								<h6>Tecnologias</h6>
								<hr/>
								@endif
								@foreach($specsTech as $specTech)
									<a href="{{action('SpecController@getTech')}}" class="thumbnail">{{Html::image('assets/img/spec/'.$specTech->img)}}</a>
								@endforeach
							</div>
							<div class="spec-opt">
								@if(count($specsOpt)>0)
								<h6>Opções</h6>
								<hr/>
								@endif
								@foreach($specsOpt as $specOpt)
									<a href="{{action('SpecController@getOptions')}}" class="thumbnail">{{Html::image('assets/img/spec/'.$specOpt->img)}}</a>
								@endforeach
							</div>
							<div class="spec-feature">
								@if(count($specsFeature)>0)
								<h6>Carateristicas</h6>
								<hr/>
								@endif
								@foreach($specsFeature as $specFeature)
									<a href="{{action('SpecController@getFeatures')}}" class="thumbnail">{{Html::image('assets/img/spec/'.$specFeature->img)}}</a>
								@endforeach
							</div>
						</div>
						<div id="tabs-4">
							@foreach($supports as $support)
								{{HTML::link(URL::to('assets/files/support/'.$support->path), $support->name, array('target'=>"_blank", 'class'=>"support-link"))}}
							@endforeach
						</div>

				</div><!--END  Tabs-->
			</div><!--END Container 6-->
		</div>
	</div><!-- END Container 12-->
  <script>
  $(function() {
    $( "#tabs" ).tabs();
    $(".product_main_image_link").magnificPopup({type:'image'});
    $("#other_images").magnificPopup({delegate:'a', type:'image'});
    
  });
  </script>
@stop
