@extends('site/layouts/master')

@section('css')
{{Html::style('assets/css/register.css')}}
@stop

{{-- Content --}}
@section('container')
		<div class="container_12">
			<div class="grid_12 complete-register">
				<h1>O seu registo foi efetuado com sucesso!</h1>
				{{HTML::linkAction('BaseController@getIndex', 'Voltar para a página Inicial',null, array('class'=>'btn-back'))}}
			</div>
		</div>
@stop
