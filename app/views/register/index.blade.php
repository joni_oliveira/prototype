@extends('site/layouts/master')

@section('css')
{{Html::style('assets/css/smoothness/jquery-ui-1.10.3.custom.css')}}
{{Html::style('assets/css/register.css')}}
@stop

@section('script')
{{Html::script('assets/js/jquery-ui-1.10.3.custom.js')}}
@stop

{{-- Content --}}
@section('container')
	<div class="container_12">
		<div class="grid_12">
			<div id="product_register">
			{{Form::open(array('action'=>'RegisterController@Create'))}}
				{{Form::label('name', 'Nome')}}
				{{Form::text('name')}}<br/>
				{{Form::label('nif', 'NIF')}}
				{{Form::text('nif')}}<br/>
				{{Form::label('product', 'Produto')}}
				{{Form::text('product')}}<br/>
				{{Form::label('serial', 'Número de Serie')}}
				{{Form::text('serial')}}<br/>
				{{Form::label('email', 'E-mail')}}
				{{Form::text('email')}}<br/>
				{{Form::label('buydate', 'Data de Compra')}}
				{{Form::text('buydate')}}<br/>
				{{Form::label('shopname', 'Nome da Loja')}}
				{{Form::text('shopname')}}<br/>
				{{Form::submit('Registar')}}
			{{Form::close()}}
			
				@if($errors->has())
			<div id="form-errors">
				<p>Ocurreram os seguintes Erros:</p>

				<ul>
					{{ $errors->first('name', '<li>:message</li>') }}
					{{ $errors->first('nif', '<li>:message</li>') }}
					{{ $errors->first('product', '<li>:message</li>') }}
					{{ $errors->first('serial', '<li>:message</li>') }}
					{{ $errors->first('email', '<li>:message</li>') }}
					{{ $errors->first('buydate', '<li>:message</li>') }}
					{{ $errors->first('shopname', '<li>:message</li>') }}
				</ul>
			</div>
				@endif
		</div>
		</div>
	</div>

  <script>
  $(function() {
    $( "#buydate" ).datepicker();
  });
  </script>
@stop

