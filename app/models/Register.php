<?php

class Register extends Eloquent {
    public static $rules = array( 
    				'name' => 'required' ,
					'nif' => 'required|numeric',
					'product' => 'required',
					'serial' => 'required|alpha_num',
					'email' => 'required|email',
					'buydate' => 'required|date',
					'shopname' => 'required');
}