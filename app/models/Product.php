<?php

use Illuminate\Support\Facades\URL;

class Product extends Eloquent
{
	public function features()
    {
        return $this->hasMany('Feature');
    }

    public function images()	
    {
    	return $this->hasMany('ProductImages');
    }

    public function getSpecsByTypeAndLimit($type = null, $limit = null){
        if(is_null($type)){
            if (is_null($limit)) {
                return DB::table('spec-product')
                ->where('product_id', '=', $this->id)
                ->join('specs', 'spec_id', '=', 'specs.id');
            }else{
                return DB::table('spec-product')
                ->where('product_id', '=', $this->id)
                ->join('specs', 'spec_id', '=', 'specs.id')
                ->take($limit);
            }
        }else{
            if (is_null($limit)) {
                return DB::table('spec-product')
                ->where('product_id', '=', $this->id)
                ->where('type', '=', $type)
                ->join('specs', 'spec_id', '=', 'specs.id');
            }else{
                return DB::table('spec-product')
                ->where('product_id', '=', $this->id)
                ->where('type', '=', $type)
                ->join('specs', 'spec_id', '=', 'specs.id')
                ->take($limit);
            }
        }
    }

    public function getSupport(){
        return DB::table('support-product')
                    ->where('product_id', '=', $this->id)
                    ->join('supports', 'support_id', '=', 'supports.id');
    }

    public function getLogo()
    {
    	return $this->images()->where('type', '=', 0)->get();
    }

    public function getMainImage()
    {
    	return $this->images()->where('type','=', 1)->get();
    }

    public function getOtherImages()
    {
    	return $this->images()->where('type', '=', 2)->get();
    }

    public function url(){
        return Url::to('product/'.$this->tiny_url);
    }

}	
