<?php

class SupportCategorie extends Eloquent
{
	protected $table = 'support-category';
	
	public function supports()
	{
		return $this->hasMany('Support', 'idCategory');
	}
}
