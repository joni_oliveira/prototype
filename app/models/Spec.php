<?php

class Spec extends Eloquent
{
	protected $table ='specs';

	/**
	 * Returns a formatted post content entry,
	 * this ensures that line breaks are returned.
	 *
	 * @return string
	 */
	public function content()
	{
		return nl2br($this->about);
	}

}
