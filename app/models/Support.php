<?php

class Support extends Eloquent
{

	public function categorie()
	{
		return $this->belongsTo('SupportCategorie', 'idCategory');
	}

}
