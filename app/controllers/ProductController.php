<?php

class ProductController extends BaseController {

    /**
     * Inject the models.
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
	 * View a blog post.
	 *
	 * @param  string  $slug
	 * @return View
	 * @throws NotFoundHttpException
	 */
	public function getProduct($slug)
	{

		$product = Product::where('tiny_url', '=', $slug)->first();

		// Check if the blog post exists
		if (is_null($product))
		{
			// If we ended up in here, it means that
			// a page or a blog post didn't exist.
			// So, this means that it is time for
			// 404 error page.
			return View::make('error/404', array('message' => 'Page Not Found'));
		}

		//get logo
		$logo = $product->getLogo();

		//get main image
		$mainimage = $product->getMainImage();

		//get other images
		$otherimages = $product->getOtherImages();

		//get main tech
		$mainspecs = $product->getSpecsByTypeAndLimit(0,3)->get();

		//get specs where type is equal 0
		$specsTech = $product->getSpecsByTypeAndLimit(0)->get();

		//get specs where type is equal 1
		$specsOpt = $product->getSpecsByTypeAndLimit(1)->get();

		//get specs where type is equal 2
		$specsFeature = $product->getSpecsByTypeAndLimit(2)->get();

		//get features
		$features = $product->features()->get();

		$supports = $product->getSupport()->get();

		$description = $product->description;

		return View::Make('product/index', array('description'=> $description,
												 'logo'=>$logo, 
												 'mainimage'=>$mainimage,
												 'otherimages'=> $otherimages,
												 'mainspecs'=>$mainspecs,
												 'specsTech'=> $specsTech,
												 'specsOpt' => $specsOpt,
												 'specsFeature' => $specsFeature,
												 'features'=>$features,
												 'supports'=>$supports));
	}
    

}
