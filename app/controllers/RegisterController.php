<?php

class RegisterController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function Index()
	{
		return View::make('register/index');
	}	

	public function Create()
	{
		$validation = Validator::make(Input::all(), Register::$rules);
		$register = new Register();

		$register->name = Input::get('name');
		$register->nif=Input::get('nif');
		$register->product=Input::get('product');
		$register->serial_number=Input::get('serial');
		$register->email= Input::get('email');
		$register->buy_date =Input::get('buydate');
		$register->shop_name = Input::get('shopname');

		if ($validation->fails()) {
			return Redirect::route('register')->withErrors($validation)->withInput();
		}else{
			$register->save();
			return View::make('register/complete');
		}
	}	
}