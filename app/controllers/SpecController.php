<?php

class SpecController extends BaseController
{

	public function getIndex()
	{
		$specs = Spec::all();

		// Show the page
		return View::make('site/spec/index', compact('specs'));
	}

	public function getTech()
	{
		$specs = Spec::where('type',0)->get();

		// Show the page
		return View::make('site/spec/index', compact('specs'));
	}

	public function getOptions()
	{
		$specs = Spec::where('type',1)->get();

		// Show the page
		return View::make('site/spec/index', compact('specs'));
	}

	public function getFeatures()
	{
		$specs = Spec::where('type',2)->get();

		// Show the page
		return View::make('site/spec/index', compact('specs'));
	}

}
