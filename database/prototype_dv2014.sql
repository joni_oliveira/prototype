-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: 23-Abr-2014 às 23:09
-- Versão do servidor: 5.5.37-cll-lve
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `prototype_dv`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `assigned_roles`
--

CREATE TABLE IF NOT EXISTS `assigned_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assigned_roles_user_id_index` (`user_id`),
  KEY `assigned_roles_role_id_index` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `assigned_roles`
--

INSERT INTO `assigned_roles` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `post_id` int(10) unsigned NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `post_id`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Lorem ipsum dolor sit amet, mutat utinam nonumy ea mel.', '2013-08-12 17:13:03', '2013-08-12 17:13:03'),
(2, 1, 1, 'Lorem ipsum dolor sit amet, sale ceteros liberavisse duo ex, nam mazim maiestatis dissentiunt no. Iusto nominavi cu sed, has.', '2013-08-12 17:13:03', '2013-08-12 17:13:03'),
(3, 1, 1, 'Et consul eirmod feugait mel! Te vix iuvaret feugiat repudiandae. Solet dolore lobortis mei te, saepe habemus imperdiet ex vim. Consequat signiferumque per no, ne pri erant vocibus invidunt te.', '2013-08-12 17:13:03', '2013-08-12 17:13:03'),
(4, 1, 2, 'Lorem ipsum dolor sit amet, mutat utinam nonumy ea mel.', '2013-08-12 17:13:03', '2013-08-12 17:13:03'),
(5, 1, 2, 'Lorem ipsum dolor sit amet, sale ceteros liberavisse duo ex, nam mazim maiestatis dissentiunt no. Iusto nominavi cu sed, has.', '2013-08-12 17:13:03', '2013-08-12 17:13:03');

-- --------------------------------------------------------

--
-- Estrutura da tabela `features`
--

CREATE TABLE IF NOT EXISTS `features` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=463 ;

--
-- Extraindo dados da tabela `features`
--

INSERT INTO `features` (`id`, `product_id`, `name`, `description`) VALUES
(1, 1, 'Material', 'Alumínio'),
(2, 1, 'Uso', 'Montanha'),
(3, 1, 'Tipo', 'Pneu'),
(4, 1, 'Furação', '28'),
(5, 1, 'Compatibilidade Travões', 'Disco'),
(6, 1, 'Medida ISO', '559x24mm'),
(7, 1, 'Altura Aro', '16mm'),
(8, 1, 'ERD', '541mm'),
(9, 1, 'Fita', '21mm'),
(10, 1, 'Pressão Max.', '50PSI'),
(11, 1, 'Pressão Min.', '1300Nm'),
(12, 1, 'Peso Max. Ciclista', '90kg'),
(13, 1, 'UST', 'Tubless Ready'),
(14, 1, 'Cor', 'Preto'),
(15, 1, 'Peso', '330gr'),
(16, 2, 'Material', 'Carbono'),
(17, 2, 'Uso', 'Montanha'),
(18, 2, 'Tipo', 'Pneu'),
(19, 2, 'Furação', '28'),
(20, 2, 'Compatibilidade Travões', 'Disco'),
(21, 2, 'Medida ISO', '559x25mm'),
(22, 2, 'Altura Aro', '24mm'),
(23, 2, 'ERD', '541mm'),
(24, 2, 'Fita', '21mm'),
(25, 2, 'Pressão Max.', '60PSI'),
(26, 2, 'Pressão Min.', '1500Nm'),
(27, 2, 'Peso Max. Ciclista', '95kg'),
(28, 2, 'UST', 'Tubless Ready'),
(29, 2, 'Cor', 'Preto'),
(30, 2, 'Peso', '320gr'),
(31, 3, 'Material', 'Alumínio'),
(32, 3, 'Uso', 'Montanha'),
(33, 3, 'Tipo', 'Pneu'),
(34, 3, 'Furação', '28-32'),
(35, 3, 'Compatibilidade Travões', 'Disco'),
(36, 3, 'Medida ISO', '584x24mm'),
(37, 3, 'Altura Aro', '17mm'),
(38, 3, 'ERD', '568mm'),
(39, 3, 'Fita', '21mm'),
(40, 3, 'Pressão Max.', '50PSI'),
(41, 3, 'Pressão Min.', '1300Nm'),
(42, 3, 'Peso Max. Ciclista', '90kg'),
(43, 3, 'UST', 'Tubless Ready'),
(44, 3, 'Cor', 'Preto'),
(45, 3, 'Peso', '365gr'),
(46, 4, 'Material', 'Carbono'),
(47, 4, 'Uso', 'Montanha'),
(48, 4, 'Tipo', 'Pneu'),
(49, 4, 'Furação', '28'),
(50, 4, 'Compatibilidade Travões', 'Disco'),
(51, 4, 'Medida ISO', '584x25mm'),
(52, 4, 'Altura Aro', '24mm'),
(53, 4, 'ERD', '568mm'),
(54, 4, 'Fita', '21mm'),
(55, 4, 'Pressão Max.', '60PSI'),
(56, 4, 'Pressão Min.', '1500Nm'),
(57, 4, 'Peso Max. Ciclista', '95kg'),
(58, 4, 'UST', 'Tubless Ready'),
(59, 4, 'Cor', 'Preto'),
(60, 4, 'Peso', '355gr'),
(61, 5, 'Material', 'Alumínio'),
(62, 5, 'Uso', 'Montanha'),
(63, 5, 'Tipo', 'Pneu'),
(64, 5, 'Furação', '32'),
(65, 5, 'Compatibilidade Travões', 'Disco'),
(66, 5, 'Medida ISO', '622x24mm'),
(67, 5, 'Altura Aro', '17mm'),
(68, 5, 'ERD', '604mm'),
(69, 5, 'Fita', '21mm'),
(70, 5, 'Pressão Max.', '50PSI'),
(71, 5, 'Pressão Min.', '1300Nm'),
(72, 5, 'Peso Max. Ciclista', '90kg'),
(73, 5, 'UST', 'Tubless Ready'),
(74, 5, 'Cor', 'Preto'),
(75, 5, 'Peso', '385gr'),
(76, 6, 'Material', 'Carbono'),
(77, 6, 'Uso', 'Montanha'),
(78, 6, 'Tipo', 'Pneu'),
(79, 6, 'Furação', '32'),
(80, 6, 'Compatibilidade Travões', 'Disco'),
(81, 6, 'Medida ISO', '622x25mm'),
(82, 6, 'Altura Aro', '24mm'),
(83, 6, 'ERD', '594mm'),
(84, 6, 'Fita', '21mm'),
(85, 6, 'Pressão Max.', '60PSI'),
(86, 6, 'Pressão Min.', '1500Nm'),
(87, 6, 'Peso Max. Ciclista', '95kg'),
(88, 6, 'UST', 'Tubless Ready'),
(89, 6, 'Cor', 'Preto'),
(90, 6, 'Peso', '360gr'),
(91, 7, 'Uso', 'Montanha'),
(92, 7, 'Aro', '26'),
(93, 7, 'Raios', 'Pillar 1.7/2.0'),
(94, 7, 'Nº Raios', '28 / 28'),
(95, 7, 'Cabeças de Raio', 'AL 18mm DSN'),
(96, 7, 'Comprimento Raios', 'F:255 T:255'),
(97, 7, 'Enraiação', '2x'),
(98, 7, 'Cubos', 'Evo3'),
(99, 7, 'Rolamentos', 'Japanese Bearing'),
(100, 7, 'Compatibilidade Travões', 'Disco'),
(101, 7, 'Compatibilidade Eixo', 'Lefty 15x100mm/12x142mm'),
(102, 7, 'Peso', '1375gr'),
(103, 7, 'Opção SL', '-50gr'),
(104, 7, 'Personalizavel', 'Sim'),
(105, 8, 'Uso', 'Montanha'),
(106, 8, 'Aro', '26'),
(107, 8, 'Raios', 'Pillar 1.5/2.0'),
(108, 8, 'Nº Raios', '28 / 28'),
(109, 8, 'Cabeças de Raio', 'AL 18mm DSN'),
(110, 8, 'Comprimento Raios', 'F:255 T:255'),
(111, 8, 'Enraiação', '2x'),
(112, 8, 'Cubos', 'Evo3'),
(113, 8, 'Rolamentos', 'Japanese Bearing'),
(114, 8, 'Compatibilidade Travões', 'Disco'),
(115, 8, 'Compatibilidade Eixo', 'Lefty 15x100mm/12x142mm'),
(116, 8, 'Peso', '1330gr'),
(117, 8, 'Opção SL', '-10gr'),
(118, 8, 'Personalizavel', 'Sim'),
(119, 9, 'Uso', 'Montanha'),
(120, 9, 'Aro', '26'),
(121, 9, 'Raios', 'Pillar X-TRA'),
(122, 9, 'Nº Raios', '28 / 28'),
(123, 9, 'Cabeças de Raio', 'AL 18mm DSN'),
(124, 9, 'Comprimento Raios', 'F:255 T:255'),
(125, 9, 'Enraiação', '2x'),
(126, 9, 'Cubos', 'Evo3'),
(127, 9, 'Rolamentos', 'ABEC 5'),
(128, 9, 'Compatibilidade Travões', 'Disco'),
(129, 9, 'Compatibilidade Eixo', 'Lefty 15x100mm/12x142mm'),
(130, 9, 'Peso', '1295gr'),
(131, 9, 'Opção SL', ''),
(132, 9, 'Personalizavel', 'Sim'),
(133, 10, 'Uso', 'Montanha'),
(134, 10, 'Aro', '26 Carbono'),
(135, 10, 'Raios', 'Pillar X-TRA'),
(136, 10, 'Nº Raios', '28 / 28'),
(137, 10, 'Cabeças de Raio', 'AL 18mm DSN'),
(138, 10, 'Comprimento Raios', 'F:255 T:255'),
(139, 10, 'Enraiação', '2x'),
(140, 10, 'Cubos', 'Evo3'),
(141, 10, 'Rolamentos', 'ABEC 5'),
(142, 10, 'Compatibilidade Travões', 'Disco'),
(143, 10, 'Compatibilidade Eixo', 'Lefty 15x100mm/12x142mm'),
(144, 10, 'Peso', ''),
(145, 10, 'Opção SL', ''),
(146, 10, 'Personalizavel', 'Sim'),
(147, 11, 'Uso', 'Montanha'),
(148, 11, 'Aro', '650B'),
(149, 11, 'Raios', 'Pillar 1.7/2.0'),
(150, 11, 'Nº Raios', '32 / 32'),
(151, 11, 'Cabeças de Raio', 'AL 18mm DSN'),
(152, 11, 'Comprimento Raios', 'F:262/268 T:262'),
(153, 11, 'Enraiação', '2x'),
(154, 11, 'Cubos', 'Evo3'),
(155, 11, 'Rolamentos', 'Japanese Bearing'),
(156, 11, 'Compatibilidade Travões', 'Disco'),
(157, 11, 'Compatibilidade Eixo', 'Lefty 15x100mm/12x142mm'),
(158, 11, 'Peso', '1480gr'),
(159, 11, 'Opção SL', '-50gr'),
(160, 11, 'Personalizavel', 'Sim'),
(161, 12, 'Uso', 'Montanha'),
(162, 12, 'Aro', '650B Carbono'),
(163, 12, 'Raios', 'Pillar X-TRA'),
(164, 12, 'Nº Raios', '28 / 28'),
(165, 12, 'Cabeças de Raio', 'AL 18mm DSN'),
(166, 12, 'Comprimento Raios', 'F:262/268 T:262'),
(167, 12, 'Enraiação', '2x'),
(168, 12, 'Cubos', 'Evo3'),
(169, 12, 'Rolamentos', 'ABEC 5'),
(170, 12, 'Compatibilidade Travões', 'Disco'),
(171, 12, 'Compatibilidade Eixo', 'Lefty 15x100mm/12x142mm'),
(172, 12, 'Peso', ''),
(173, 12, 'Opção SL', ''),
(174, 12, 'Personalizavel', 'Sim'),
(175, 13, 'Uso', 'Montanha'),
(176, 13, 'Aro', '29'),
(177, 13, 'Raios', 'Pillar 1.7/2.0'),
(178, 13, 'Nº Raios', '32 / 32'),
(179, 13, 'Cabeças de Raio', 'AL 18mm DSN'),
(180, 13, 'Comprimento Raios', 'F:280/285 T:280'),
(181, 13, 'Enraiação', '2x'),
(182, 13, 'Cubos', 'Evo3'),
(183, 13, 'Rolamentos', 'Japanese Bearing'),
(184, 13, 'Compatibilidade Travões', 'Disco'),
(185, 13, 'Compatibilidade Eixo', 'Lefty 15x100mm/12x142mm'),
(186, 13, 'Peso', '1530gr'),
(187, 13, 'Opção SL', '-55gr'),
(188, 13, 'Personalizavel', 'Sim'),
(189, 14, 'Uso', 'Montanha'),
(190, 14, 'Aro', '29 Carbono'),
(191, 14, 'Raios', 'Pillar X-TRA'),
(192, 14, 'Nº Raios', '28 / 28'),
(193, 14, 'Cabeças de Raio', 'AL 18mm DSN'),
(194, 14, 'Comprimento Raios', 'F:280/285 T:280'),
(195, 14, 'Enraiação', '2x'),
(196, 14, 'Cubos', 'Evo3'),
(197, 14, 'Rolamentos', 'ABEC 5'),
(198, 14, 'Compatibilidade Travões', 'Disco'),
(199, 14, 'Compatibilidade Eixo', 'Lefty 15x100mm/12x142mm'),
(200, 14, 'Peso', ''),
(201, 14, 'Opção SL', ''),
(202, 14, 'Personalizavel', 'Sim'),
(203, 15, 'Material', 'AL Light-Force'),
(204, 15, 'Medidas Disponíveis', '27.2; 30,9; 31.6'),
(205, 15, 'Comprimento Máximo', '410 mm'),
(206, 15, 'Comprimento Mínimo de Inserção', '100 (410) mm'),
(207, 15, 'Aperto Máximo', '?? Nm'),
(208, 15, 'Peso Max. Ciclista', '90 kg'),
(209, 15, 'Peso', '180gr (31,6x410)'),
(210, 20, 'Material', 'AL Light-Force '),
(211, 20, 'Comprimento máximo', '75 mm'),
(212, 20, 'Ângulo de Convergência', '9.5º '),
(213, 20, 'Aperto', '2-5 Nm'),
(214, 20, 'Peso', '46 gr'),
(215, 20, 'Peso Máximo atleta', 'Sem Limite'),
(216, 20, 'Garantia', '4 Anos'),
(217, 21, 'Material', 'Aço'),
(218, 21, 'Interior Removível', 'Sim'),
(219, 21, 'Cor', 'Cromado'),
(221, 22, 'Cor', 'Branco'),
(222, 23, 'Material', 'Alumínio Light Force'),
(223, 23, 'Compatibilidade', 'Shimano / SRAM T11'),
(224, 23, 'Cor', 'Azul, Dourado, Preto, Vermelho, Verde'),
(225, 23, 'Peso', '5.8gr'),
(226, 23, 'Garantia', '2 anos'),
(227, 25, 'Material', 'Alumínio Light Force'),
(228, 25, 'Número de dentes', '11'),
(229, 25, 'Cor', 'Azul, Dourado, Preto, Vermelho, Verde'),
(230, 25, 'Peso', 'xxgr'),
(231, 21, 'Peso', '7,4gr'),
(232, 22, 'Dimensão', '21mm x 5m'),
(233, 22, 'Pressão Máxima', '8 bar'),
(234, 22, 'Peso Roda 26', '4-5gr'),
(235, 22, 'Garantia', '2 Anos'),
(236, 22, 'Garantia', '2 Anos'),
(237, 26, 'Material', 'Alumínio Light Force'),
(238, 26, 'Interior Removivel', 'Sim'),
(239, 26, 'Cor', 'Preto, Vermelho'),
(240, 26, 'Peso', '4,2gr'),
(241, 26, 'Garantia', '2 Anos'),
(242, 27, 'Tamanho', '700C / 38mm'),
(243, 27, 'Tipo', 'Pneu'),
(244, 27, 'Utilização', 'Estrada / Ciclocross'),
(245, 27, 'Aro', '100% Carbono'),
(246, 27, 'Raios', 'AERO'),
(247, 27, 'Nº Raios', 'F:20 T:24'),
(248, 27, 'Rolamentos', 'Rolamentos Selados'),
(249, 27, 'Válvula (mm, min.)', '50'),
(250, 27, 'Cubos', 'Speed'),
(251, 27, 'Compatibilidade Eixo', 'F:100 T:130'),
(252, 27, 'Comp. Travão', 'Pinça / Cantilever'),
(253, 27, 'Peso do Atleta (kg)', '95'),
(254, 27, 'Peso Total (g)', '1420'),
(255, 28, 'Tamanho', '700C / 38mm'),
(256, 28, 'Tipo', 'Tubular'),
(257, 28, 'Utilização', 'Estrada / Ciclocross'),
(258, 28, 'Aro', '100% Carbono'),
(259, 28, 'Raios', 'AERO'),
(260, 28, 'Nº Raios', 'F:20 T:24'),
(261, 28, 'Rolamentos', 'Rolamentos Selados'),
(262, 28, 'Válvula (mm, min.)', '50'),
(263, 28, 'Cubos', 'Speed'),
(264, 28, 'Compatibilidade Eixo', 'F:100 T:130'),
(265, 28, 'Comp. Travão', 'Pinça / Cantilever'),
(266, 28, 'Peso do Atleta (kg)', '95'),
(267, 28, 'Peso Total (g)', '1250'),
(268, 29, 'Tamanho', '700C / 50mm'),
(269, 29, 'Tipo', 'Pneu'),
(270, 29, 'Utilização', 'Estrada / Ciclocross'),
(271, 29, 'Aro', '100% Carbono'),
(272, 29, 'Raios', 'AERO'),
(273, 29, 'Nº Raios', 'F:20 T:24'),
(274, 29, 'Rolamentos', 'Rolamentos Selados'),
(275, 29, 'Válvula (mm, min.)', '60'),
(276, 29, 'Cubos', 'Speed'),
(277, 29, 'Compatibilidade Eixo', 'F:100 T:130'),
(278, 29, 'Comp. Travão', 'Pinça / Cantilever'),
(279, 29, 'Peso do Atleta (kg)', '110'),
(280, 29, 'Peso Total (g)', '1510'),
(281, 30, 'Tamanho', '700C / 50mm'),
(282, 30, 'Tipo', 'Tubular'),
(283, 30, 'Utilização', 'Estrada / Ciclocross'),
(284, 30, 'Aro', '100% Carbono'),
(285, 30, 'Raios', 'AERO'),
(286, 30, 'Nº Raios', 'F:20 T:24'),
(287, 30, 'Rolamentos', 'Rolamentos Selados'),
(288, 30, 'Válvula (mm, min.)', '60'),
(289, 30, 'Cubos', 'Speed'),
(290, 30, 'Compatibilidade Eixo', 'F:100 T:130'),
(291, 30, 'Comp. Travão', 'Pinça / Cantilever'),
(292, 30, 'Peso do Atleta (kg)', '110'),
(293, 30, 'Peso Total (g)', '1360'),
(294, 31, 'Tamanho', '700C / 20mm'),
(295, 31, 'Tipo', 'Tubular'),
(296, 31, 'Utilização', 'Estrada / Ciclocross'),
(297, 31, 'Aro', '100% Carbono'),
(298, 31, 'Raios', 'AERO'),
(299, 31, 'Nº Raios', 'F:20 T:24'),
(300, 31, 'Rolamentos', 'Rolamentos Selados'),
(301, 31, 'Válvula (mm, min.)', '30'),
(302, 31, 'Cubos', 'Speed'),
(303, 31, 'Compatibilidade Eixo', 'F:100 T:130'),
(304, 31, 'Comp. Travão', 'Pinça / Cantilever'),
(305, 31, 'Peso do Atleta (kg)', '75'),
(306, 31, 'Peso Total (g)', '990'),
(307, 32, 'Tamanho', '700C / 38mm'),
(308, 32, 'Tipo', 'Tubular'),
(309, 32, 'Utilização', 'Estrada / Ciclocross'),
(310, 32, 'Aro', '100% Carbono'),
(311, 32, 'Raios', 'AERO'),
(312, 32, 'Nº Raios', 'F:20 T:24'),
(313, 32, 'Rolamentos', 'Rolamentos Selados'),
(314, 32, 'Válvula (mm, min.)', '50'),
(315, 32, 'Cubos', 'Speed'),
(316, 32, 'Compatibilidade Eixo', 'F:100 T:130'),
(317, 32, 'Comp. Travão', 'Pinça / Cantilever'),
(318, 32, 'Peso do Atleta (kg)', '85'),
(319, 32, 'Peso Total (g)', '1080'),
(320, 33, 'Tamanho', '700C / 50mm'),
(321, 33, 'Tipo', 'Tubular'),
(322, 33, 'Utilização', 'Estrada / Ciclocross'),
(323, 33, 'Aro', '100% Carbono'),
(324, 33, 'Raios', 'AERO'),
(325, 33, 'Nº Raios', 'F:20 T:24'),
(326, 33, 'Rolamentos', 'Rolamentos Selados'),
(327, 33, 'Válvula (mm, min.)', '60'),
(328, 33, 'Cubos', 'Speed'),
(329, 33, 'Compatibilidade Eixo', 'F:100 T:130'),
(330, 33, 'Comp. Travão', 'Pinça / Cantilever'),
(331, 33, 'Peso do Atleta (kg)', '90'),
(332, 33, 'Peso Total (g)', '1120'),
(333, 34, 'Material', 'Alumínio Light Force'),
(334, 34, 'Medida', '5 + 10 mm (set)'),
(335, 34, 'Cor', 'Azul Escuro, Dourado, Preto, Vermelho, Verde'),
(336, 34, 'Peso 5mm', '1,7 gr'),
(337, 34, 'Peso 10mm', '3,5 gr'),
(338, 34, 'Garantia', '2 Anos'),
(339, 35, 'Material', 'Alumínio Light Force'),
(340, 35, 'Medida', '42 x 36,1 x 2,5 mm'),
(341, 35, 'Cor', 'Dourado, Preto, Vermelho'),
(342, 35, 'Unidades Embalagem', '3'),
(343, 35, 'Peso', '2,4 gr (unid)'),
(344, 35, 'Garantia', '2 Anos'),
(345, 36, 'Material', 'Alumínio Light Force'),
(346, 36, 'Medida', '12 x 8 x 1 mm (Vermelho) / 13 x 10 x 1 mm (Preto)'),
(347, 36, 'Unidades Embalagem', '4'),
(348, 36, 'Peso', '2,4 gr (4 unid)'),
(349, 36, 'Garantia', '2 Anos'),
(350, 37, 'Material', 'Alumínio Light Force'),
(351, 37, 'Medidas', '31,8mm / 34,9mm / 38,2 mm'),
(352, 37, 'Cor', 'Azul, Dourado, Preto, Vermelho, Verde'),
(354, 37, 'Peso 31,8mm', '5,4 gr'),
(355, 37, 'Peso 34,9mm', '5,6 gr'),
(356, 37, 'Peso 38,2mm', '5,9 gr'),
(357, 37, 'Garantia', '2 Anos'),
(358, 38, 'Modelo Shimano Dura-Ace SL', '55,5mm - 2 gr'),
(359, 38, 'Modelo Shimano XTR', '86.5mm - 9 gr'),
(360, 38, 'Modelo Shimano XTR', '99.5mm - 8 gr'),
(361, 38, 'Modelo Sram Red / Force', '55mm - 5 gr'),
(362, 38, 'Modelo Sram XX/X0/X9', '93mm - 8 gr'),
(363, 38, 'Material', 'Carbono 3k'),
(364, 38, 'Garantia', '2 Anos'),
(365, 39, 'Material', 'Alumínio Light Force'),
(366, 39, 'Compatibilidade', 'Shimano BSA'),
(367, 39, 'Rolamentos', 'Black Shield'),
(368, 39, 'Rolamentos substituíveis', 'Sim'),
(369, 39, 'Cor', 'Preto, Vermelho, Dourado'),
(370, 39, 'Peso', '85 gr'),
(371, 39, 'Garantia', '2 Anos'),
(372, 40, 'Material', 'Alumínio Light Force'),
(373, 40, 'Compatibilidade', 'GXP'),
(374, 40, 'Rolamentos', 'Black Shield'),
(375, 40, 'Rolamentos substituíveis', 'Sim'),
(376, 40, 'Cor', 'Preto, Vermelho, Dourado'),
(377, 40, 'Peso', '120 gr'),
(378, 40, 'Garantia', '2 Anos'),
(379, 41, 'Material', 'Alumínio Light Force'),
(380, 41, 'Rosca', 'M5'),
(381, 41, 'Aperto Máximo', '5 Nm'),
(382, 41, 'Comprimentos', '10, 16, 20 mm'),
(383, 41, 'Cor', 'Azul, Dourado, Preto, Vermelho, Verde, Rosa'),
(384, 41, 'Peso M5 x 10mm', '1,0 gr'),
(385, 41, 'Peso M5 x 16mm', '1,3 gr'),
(386, 41, 'Peso M5 x 20mm', '1,4 gr'),
(387, 41, 'Garantia', '2 Anos'),
(388, 42, 'Material', 'Alumínio Light Force'),
(389, 42, 'Rosca', 'M6'),
(390, 42, 'Aperto Máximo', '6 Nm'),
(391, 42, 'Comprimentos', '15, 20, 25, 35 mm'),
(392, 42, 'Cor', 'Azul, Dourado, Preto, Vermelho, Verde, Rosa'),
(393, 42, 'Peso M6 x 15mm', '1,6 gr'),
(394, 42, 'Peso M6 x 20mm', '1,8 gr'),
(395, 42, 'Peso M6 x 25mm', '2,0 gr'),
(396, 42, 'Peso M6 x 35mm', '2,8 gr'),
(397, 42, 'Garantia', '2 Anos'),
(398, 43, 'Material', 'Alumínio Light Force'),
(399, 43, 'Medidas', '6.5 / 8.5 mm'),
(400, 43, 'Aperto Máximo', '5 Nm'),
(401, 43, 'Unidades por Blister', '4 / 5 pcs'),
(402, 43, 'Cor', 'Azul, Dourado, Preto, Vermelho, Verde'),
(403, 43, 'Peso 6.5mm', '5.8 gr (4 sets)'),
(404, 43, 'Peso 8.5mm', '6.8 gr (4 sets)'),
(405, 43, 'Garantia', '2 Anos'),
(406, 44, 'Medida', '30x41 x 6.5 x 45º x 45º 1-1/8'),
(407, 44, 'Medida', '40x51,8 x 8 x 45º x 45º  1,5"'),
(408, 44, 'Medida', '30 x 41,8 x 8 x 45º x 45º 1-1/8'),
(409, 44, 'Medida', '17 x 26 x 5 6803'),
(410, 44, 'Medida', '15 x 28 x 7 6902'),
(411, 44, 'Medida', '15 x 24 x 5 6802'),
(412, 44, 'Medida', '25 x 37 x 7 6805'),
(413, 44, 'Medida', '30 x 42 x 7 6806'),
(414, 44, 'Medida', '22 x 36 x 7/15'),
(415, 44, 'Medida', '10 x 22 x 6 6900'),
(416, 44, 'Medida', '19/16x2x9"'),
(417, 45, 'Material', 'Alumínio Light Force'),
(418, 45, 'Rosca', 'M5'),
(419, 45, 'Aperto Máximo', '4 Nm'),
(420, 45, 'Comprimentos', '10 mm'),
(421, 45, 'Cor', 'Azul, Dourado, Preto, Vermelho, Verde, Rosa'),
(422, 45, 'Chave Montagem Incluída', 'Sim'),
(423, 45, 'Unidades por Blister', '12 pcs'),
(424, 45, 'Peso', '9,6 gr (12 pcs)'),
(425, 45, 'Garantia', '2 Anos'),
(426, 46, 'Material Corpo', 'AL 6061 T6'),
(427, 46, 'Material Tampas', 'AL 6061 T6'),
(428, 46, 'Rolamentos', '2x6803'),
(429, 46, 'Tipo Rolamentos', 'Rolamentos Selados'),
(430, 46, 'Furos', '20/24x(2.5x3.2)'),
(431, 46, 'Compatibilidade', '9x100mm'),
(432, 46, 'Cor', 'Preto, Branco'),
(433, 46, 'Peso', '80gr'),
(434, 47, 'Material Corpo', 'AL 6061 T6'),
(435, 47, 'Material Tampas', 'AL 6061 T6'),
(436, 47, 'Material Cepo', 'AL 6061 T6'),
(437, 47, 'Rolamentos', '1x6803, 3x6902'),
(438, 47, 'Tipo Rolamentos', 'Rolamentos Selados'),
(439, 47, 'Engrenagem Cepo', '3x Linguetes de aço endurecido'),
(440, 47, 'Furos', '20/24 x (2.5x3.2)'),
(441, 47, 'Compatibilidade', '9x100mm'),
(442, 47, 'Cor', 'Preto, Branco'),
(443, 47, 'Peso', '80gr'),
(444, 48, 'Material', 'Alumínio Light Force'),
(445, 48, 'Comprimento Máximo', '580-660mm / 670-710mm'),
(446, 48, 'Compatibilidade Extensores', 'Sim'),
(447, 48, 'Ângulo', '5º/10º'),
(448, 48, 'Aperto Máximo Extremidades', '3-5 Nm'),
(449, 48, 'Aperto Máximo', '6 Nm'),
(450, 48, 'Peso Máximo Atletas', '100 kg'),
(451, 48, 'Peso', '215gr (660mm)'),
(452, 48, 'Garantia', '4 Anos'),
(453, 49, 'Material', 'Alumínio Light Force / Titânio'),
(454, 49, 'Ângulo', '6º/17º'),
(455, 49, 'Comprimento Máximo', '80 - 110mm'),
(456, 49, 'Diâmetro Guiador', '31,8mm'),
(457, 49, 'Aperto Máximo Avanço', '5 Nm'),
(458, 49, 'Cor', 'Preto'),
(459, 49, 'Peso Máximo Atletas', '100 kg'),
(460, 49, 'Peso', '98gr (6º 90mm) / 142gr (17º 90mm)'),
(461, 49, 'Garantia', '4 Anos'),
(462, 50, 'Embalagens:', '75ml, 150ml, 500ml e 5L');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2013_07_18_001629_create_product_register_table', 1),
('2013_07_20_145900_create_register_table', 2),
('2013_07_22_214644_create_features_table', 3),
('2013_07_22_214734_create_posts_table', 3),
('2013_07_22_214750_create_posts-category_table', 3),
('2013_07_22_214816_create_products_table', 3),
('2013_07_22_214846_create_products-category_table', 3),
('2013_07_22_214952_create_product-images_table', 3),
('2013_07_22_220403_create_features_table', 4),
('2013_07_22_221255_create_posts_table', 4),
('2013_07_22_221315_create_posts-category_table', 4),
('2013_07_22_221432_create_products_table', 4),
('2013_07_22_221503_create_products-category_table', 4),
('2013_07_22_221532_create_product-images_table', 4),
('2013_07_22_221912_create_support_table', 4),
('2013_07_22_222107_create_support-category_table', 4),
('2013_07_22_222213_create_tech_table', 4),
('2013_07_22_222234_create_tech-product_table', 4),
('2013_07_30_214838_create_slides_table', 5),
('2013_02_05_024934_confide_setup_users_table', 1),
('2013_02_05_043505_create_posts_table', 1),
('2013_02_05_044505_create_comments_table', 1),
('2013_02_08_031702_entrust_setup_tables', 1),
('2013_05_21_024934_entrust_permissions', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_reminders`
--

CREATE TABLE IF NOT EXISTS `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`),
  UNIQUE KEY `permissions_display_name_unique` (`display_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`) VALUES
(1, 'manage_blogs', 'manage blogs'),
(2, 'manage_posts', 'manage posts'),
(3, 'manage_comments', 'manage comments'),
(4, 'manage_users', 'manage users'),
(5, 'manage_roles', 'manage roles'),
(6, 'post_comment', 'post comment');

-- --------------------------------------------------------

--
-- Estrutura da tabela `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_role_permission_id_role_id_unique` (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 6, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `posts-category_id` int(11) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `posts-category_id` (`posts-category_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `posts`
--

INSERT INTO `posts` (`id`, `posts-category_id`, `user_id`, `title`, `slug`, `content`, `img`, `meta_title`, `meta_description`, `meta_keywords`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Ivo Santos Wins With NEW Prototype Protour 2SL', 'ivo-santos-wins-prototype-2-sl', 'Ivo Santos vence IV Grande Prémio Liberty Seguros - Volta a S. Miguel, Açores.\n\nEstabeleceu um novo recorde na subida ao Lago do Fogo utilizando as novas PROTOTYPE PRO TOUR 2 SL\n\n', 'ivo-santos.png', 'meta_title1', 'meta_description1', 'meta_keywords1', '2013-08-12 16:13:03', '2013-08-12 16:13:03'),
(2, 2, 1, 'Festival Bike', 'festibike', 'De 18 a 20 de Outubro todos os caminhos vão dar ao Centro Nacional de Exposições e durante três dias, Santarém é a “capital” da Bicicleta no nosso país. \nVisite o stand da PROTOTYPE e conheça todas as novas novidades e veja a gama disponível para 2014.', 'stand_prototype.jpg', 'meta_title2', 'meta_description2', 'meta_keywords2', '2013-08-12 16:13:03', '2013-08-12 16:13:03');

-- --------------------------------------------------------

--
-- Estrutura da tabela `posts-category`
--

CREATE TABLE IF NOT EXISTS `posts-category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `posts-category`
--

INSERT INTO `posts-category` (`id`, `name`) VALUES
(1, 'Product'),
(2, 'News');

-- --------------------------------------------------------

--
-- Estrutura da tabela `product-images`
--

CREATE TABLE IF NOT EXISTS `product-images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=206 ;

--
-- Extraindo dados da tabela `product-images`
--

INSERT INTO `product-images` (`id`, `product_id`, `name`, `path`, `type`) VALUES
(1, 1, 'Rim 26R', 'rim_26.jpg', '1'),
(2, 1, 'Rim 26R Lateral', 'rim_26_lateral.jpg', '2'),
(3, 2, 'Rim 26R Carbon', 'rim_26_carbon.jpg', '1'),
(4, 2, 'Rim 26R Carbon Lateral', 'rim_26_carbon_lateral.jpg', '2'),
(5, 3, 'Logo 650B', '650b-logo.png', '0'),
(6, 3, 'Rim 650B', 'rim_650.jpg', '1'),
(7, 3, 'Rim 650B Lateral', 'rim_650_lateral.jpg', '2'),
(8, 4, 'Rim 650B Carbon', 'rim_650_carbon.jpg', '1'),
(9, 4, 'Rim 650B Carbon Lateral', 'rim_650_carbon_lateral.jpg', '2'),
(10, 5, 'Rim 29R', 'rim_29.jpg', '1'),
(11, 5, 'Rim 29R Lateral', 'rim_29_lateral.jpg', '2'),
(12, 6, 'Rim 29R Carbon', 'rim_29_carbon.jpg', '1'),
(13, 6, 'Rim 29R Carbon Lateral', 'rim_29_carbon_lateral.jpg', '2'),
(14, 13, 'Wheel 29 R Back Wheel', 'wheel_29_bw.jpg', '1'),
(15, 13, 'Wheel 29 R Back Wheel Lateral', 'wheel_29_bw_lateral.jpg', '2'),
(16, 13, 'Wheel 29 R Front Wheel', 'wheel_29_fw.jpg', '2'),
(17, 13, 'Wheel 29 R Front Wheel Lateral', 'wheel_29_fw_lateral.jpg', '2'),
(18, 11, 'Wheel 650B Back Wheel', 'wheel_650b_bw.jpg', '1'),
(19, 11, 'Wheel 650B Back Wheel Lateral', 'wheel_650b_bw_lateral.jpg', '2'),
(20, 11, 'Wheel 650B Front Wheel', 'wheel_650b_fw.jpg', '2'),
(21, 11, 'Wheel 650B Front Wheel Lateral', 'wheel_650b_fw_lateral.jpg', '2'),
(22, 9, 'Wheel Marathon Back Wheel', 'wheel_marathon_bw.jpg', '1'),
(23, 9, 'Wheel Marathon Back Wheel Lateral', 'wheel_marathon_bw_lateral.jpg', '2'),
(24, 9, 'Wheel Marathon Front Wheel', 'wheel_marathon_fw.jpg', '2'),
(25, 9, 'Wheel Marathon Front Wheel Lateral', 'wheel_marathon_fw_lateral.jpg', '2'),
(26, 11, 'Logo 650B', '650b-logo.png', '0'),
(27, 1, 'Logo Rim 26R', 'race-logo.png', '0'),
(28, 2, 'Logo 26R Carbon', '26r_carbon-logo.png', '0'),
(29, 4, 'Logo 650B Carbon', '650b_carbon-logo.png', '0'),
(30, 5, 'Logo 29R', '29r-logo.png', '0'),
(31, 6, 'Logo 29R Carbon', '29r_carbon-logo.png', '0'),
(32, 7, 'Logo Race', 'race-logo.png', '0'),
(33, 8, 'Logo Cross', 'cross-logo.png', '0'),
(34, 9, 'Logo Marathon', 'marathon-logo.png', '0'),
(35, 10, 'Logo 26R Carbon', '26r_carbon-logo.png', '0'),
(36, 12, 'Logo 650B Carbon', '650b_carbon-logo.png', '0'),
(37, 13, 'Logo 29R', '29r-logo.png', '0'),
(38, 14, 'Logo 29R Carbon', '29r_carbon-logo.png', '0'),
(39, 7, 'Exemplo', 'wheel_exemplo.jpg', '1'),
(40, 7, 'Exemplo Lateral', 'wheel_exemplo_lateral.jpg', '2'),
(41, 8, 'Exemplo', 'wheel_exemplo.jpg', '1'),
(42, 8, 'Exemplo Lateral', 'wheel_exemplo_lateral.jpg', '2'),
(43, 10, 'Exemplo', 'wheel_exemplo.jpg', '1'),
(44, 10, 'Exemplo Lateral', 'wheel_exemplo_lateral.jpg', '2'),
(45, 12, 'Exemplo', 'prototype_650bc_f_bk_01.jpg', '1'),
(46, 12, 'Exemplo Lateral', 'prototype_650bc_f_bk.jpg', '2'),
(47, 14, 'Wheel 29R Carbon', 'prototype_29carbon_t_bk.jpg', '1'),
(48, 14, 'Wheel 29RCarbon Lateral', 'prototype_29carbon_f1_bk.jpg', '2'),
(49, 15, 'Logo Physis', 'physis.png', '0'),
(50, 15, 'Seat Post Physis Black', 'prototype_sp_bk.jpg', '1'),
(51, 15, 'Seat Post Physis Green', 'prototype_sp_gr.jpg', '2'),
(52, 16, 'Logo Evo3', 'evo3.png', '0'),
(53, 17, 'Logo Evo3', 'evo3.png', '0'),
(54, 18, 'Logo Evo3 Lefty', 'evo3_lefty-logo.png', '0'),
(55, 18, 'Evo 3 Lefty Black', 'prototype_hub_evo3_lefty_bk.jpg', '1'),
(56, 18, 'Evo3 Lefty White', 'prototype_hub_evo3_lefty_wh.jpg', '2'),
(57, 16, 'Evo 3 Front Black', 'prototype_hub_evo3_f_bk.jpg', '1'),
(58, 16, 'Evo3 Front White', 'prototype_hub_evo3_f_wh.jpg', '2'),
(59, 17, 'Evo3 Rear White', 'prototype_hub_evo3_r_wh.jpg', '1'),
(60, 17, 'Evo 3 Black', 'prototype_hub_evo3_r_bk.jpg', '2'),
(61, 20, 'Logo Physis', 'physis.png', '0'),
(62, 20, 'Physis Bar End Red', 'prototype_physis_barend_rd.jpg', '1'),
(63, 20, 'Physis Bar End Black', 'prototype_physis_barend_bk.jpg', '2'),
(64, 20, 'Physis Bar End Blue', 'prototype_physis_barend_bl.jpg', '2'),
(65, 20, 'Physis Bar End Gold', 'prototype_physis_barend_gd.jpg', '2'),
(66, 20, 'Physis Bar End Green', 'prototype_physis_barend_gr.jpg', '2'),
(67, 22, 'Fita UST', 'prototype_ust_tape.jpg', '1'),
(68, 22, 'Fita UST e Valvula', 'prototype_ust_tape2.jpg', '2'),
(69, 22, 'Logo Fita Prototype', 'prototype-logo.png', '0'),
(70, 14, 'Wheel 29R Carbon Front', 'prototype_29r_f_bk.jpg', '2'),
(71, 23, 'Logo Aperto Cassete Prototype', 'prototype-logo.png', '0'),
(72, 23, 'Roldana red', 'prototype_lock11_rd.jpg', '1'),
(73, 23, 'Aperto Cassete Verde', 'prototype_lock11_gr.jpg', '2'),
(74, 23, 'Aperto Cassete Dourado', 'prototype_lock11_gd.jpg', '2'),
(75, 23, 'Aperto Cassete Azul', 'prototype_lock11_bl.jpg', '2'),
(76, 23, 'Aperto Cassete Preto', 'prototype_lock11_bk.jpg', '2'),
(77, 24, 'Logo Prototype Bidao', 'prototype-logo.png', '0'),
(78, 24, 'Bidão 600ml', 'prototype_bottle_600ml.jpg', '1'),
(79, 25, 'Logo Prototype Roldanas', 'prototype-logo.png', '0'),
(80, 25, 'Roldana Preto', 'prototype_jockey_wheels_al_bk.jpg', '1'),
(81, 25, 'Roldana Azul', 'prototype_jockey_wheels_al_bl.jpg', '2'),
(82, 25, 'Roldana Dourado', 'prototype_jockey_wheels_al_gd.jpg', '2'),
(83, 25, 'Roldana Verde', 'prototype_jockey_wheels_al_gr.jpg', '2'),
(84, 21, 'Logo Valvula Prototype', 'prototype-logo.png', '0'),
(85, 21, 'Valvulas Prototype', 'prototype_valve.jpg', '1'),
(86, 26, 'Logo Prototype', 'prototype-logo.png', '0'),
(87, 26, 'Logo Prototype', 'prototype_valve.jpg', '1'),
(88, 27, 'Logo Pro Tour 3', 'protour_3-logo.png', '0'),
(89, 27, 'Logo Pro Tour 3 Clincher Front White', 'prototype_pt3c_f1_wh.jpg', '1'),
(90, 27, 'Logo Pro Tour 3 Clincher Rear White', 'prototype_pt3c_t1_wh.jpg', '2'),
(91, 27, 'Logo Pro Tour 3 Front White', 'prototype_pt3_f1_wh.jpg', '2'),
(92, 27, 'Logo Pro Tour 3 Rear White', 'prototype_pt3_t1_wh.jpg', '2'),
(93, 27, 'Logo Pro Tour 3 Rear Black', 'prototype_pt3_t1_bk.jpg', '2'),
(94, 28, 'Logo Pro Tour 3', 'protour_3-logo.png', '0'),
(95, 28, 'Logo Pro Tour 3 Tubular Front White', 'prototype_pt3t_f1_wh.jpg', '1'),
(96, 28, 'Logo Pro Tour 3 Tubular Rear White', 'prototype_pt3t_t1_wh.jpg', '2'),
(97, 28, 'Logo Pro Tour 3 Front White', 'prototype_pt3_f1_wh.jpg', '2'),
(98, 28, 'Logo Pro Tour 3 Rear White', 'prototype_pt3_t1_wh.jpg', '2'),
(99, 28, 'Logo Pro Tour 3 Rear Black', 'prototype_pt3_t1_bk.jpg', '2'),
(100, 29, 'Logo Pro Tour 5', 'protour_5-logo.png', '0'),
(101, 29, 'Logo Pro Tour 5 Front Black', 'prototype_pt5_f1_bk.jpg', '1'),
(102, 29, 'Logo Pro Tour 5 Rear White', 'prototype_pt5_raios_bk.jpg', '2'),
(103, 29, 'Logo Pro Tour 5 Rear Black', 'prototype_pt5_t1_bk.jpg', '2'),
(104, 30, 'Logo Pro Tour 5', 'protour_5-logo.png', '0'),
(105, 30, 'Logo Pro Tour 5 Front Black', 'prototype_pt5_f1_bk.jpg', '1'),
(106, 30, 'Logo Pro Tour 5 Rear White', 'prototype_pt5_raios_bk.jpg', '2'),
(107, 30, 'Logo Pro Tour 5 Rear Black', 'prototype_pt5_t1_bk.jpg', '2'),
(108, 30, 'Logo Pro Tour 5 Tubular Front Black', 'prototype_pt5t_f1.jpg', '2'),
(109, 30, 'Logo Pro Tour 5 Tubular Rear White', 'prototype_pt5t_t1.jpg', '2'),
(110, 31, 'Logo Pro Tour 2 SL', 'protour_2sl-logo.png', '0'),
(111, 31, 'Logo Pro Tour 3 Tubular Front White', 'prototype_pt3t_f1.jpg', '1'),
(112, 32, 'Logo Pro Tour 3 SL', 'protour_3sl-logo.png', '0'),
(113, 32, 'Logo Pro Tour 3 Tubular Front White', 'prototype_pt3t_f1.jpg', '1'),
(114, 33, 'Logo Pro Tour 5 SL', 'protour_5sl-logo.png', '0'),
(115, 33, 'Logo Pro Tour 5 Tubular', 'prototype_5sl_f_00.jpg', '1'),
(116, 36, 'Logo Prototype', 'prototype-logo.png', '0'),
(117, 36, 'Anilhas Cranck Prototype', 'prototype_spacer_cr_8.jpg', '1'),
(118, 35, 'Logo Prototype', 'prototype-logo.png', '0'),
(119, 35, 'Anilhas BB Prototype', 'prototype_spacer_bb_1.jpg', '1'),
(120, 34, 'Logo Prototype', 'prototype-logo.png', '0'),
(121, 34, 'Anilhas Direção Prototype Vermelho', 'prototype_spacer_rd.jpg', '1'),
(122, 34, 'Anilhas Direção Prototype Verde', 'prototype_spacer_gr.jpg', '2'),
(123, 34, 'Anilhas Direção Prototype Dourado', 'prototype_spacer_gd.jpg', '2'),
(124, 34, 'Anilhas Direção Prototype Azul', 'prototype_spacer_bl.jpg', '2'),
(125, 34, 'Anilhas Direção Prototype Preto', 'prototype_spacer_bk.jpg', '2'),
(126, 37, 'Logo Prototype', 'prototype-logo.png', '0'),
(127, 37, 'Aperto Espigão Preto', 'prototype_seatclamp_bk.jpg', '1'),
(128, 37, 'Aperto Espigão Azul', 'prototype_seatclamp_bl.jpg', '2'),
(129, 37, 'Aperto Espigão Dourado', 'prototype_seatclamp_gd.jpg', '2'),
(130, 37, 'Aperto Espigão Verde', 'prototype_seatclamp_gr.jpg', '2'),
(131, 33, 'Pro Tour SL 5', 'prototype_5sl_f01.jpg', '2'),
(132, 38, 'Logo Prototype', 'prototype-logo.png', '0'),
(133, 38, 'Caixa Dura-Ace', 'prototype_0462_sh_555sl.jpg', '1'),
(134, 38, 'Caixa XTR', 'prototype_0476_xtr_995.jpg', '2'),
(135, 38, 'Caixa XX', 'prototype_5024_xx_93.jpg', '2'),
(136, 38, 'Caixa Red', 'prototype_5025_red_55.jpg', '2'),
(137, 39, 'Logo Prototype', 'prototype-logo.png', '0'),
(138, 39, 'Prototype BB BSA Preto', 'prototype_bb_bsa_bk.jpg', '1'),
(139, 39, 'Prototype BB BSA Vermelho', 'prototype_bb_bsa_red.jpg', '2'),
(140, 39, 'Prototype BB BSA Dourado', 'prototype_bb_bsa_gd.jpg', '2'),
(141, 40, 'Logo Prototype', 'prototype-logo.png', '0'),
(142, 40, 'Prototype BB GXP Preto', 'prototype_bb_gxp_bk.jpg', '1'),
(143, 40, 'Prototype BB GXP Vermelho', 'prototype_bb_gxp_red.jpg', '2'),
(144, 40, 'Prototype BB GXP Dourado', 'prototype_bb_gxp_gd.jpg', '2'),
(145, 41, 'Logo Prototype', 'prototype-logo.png', '0'),
(146, 41, 'Parafuso M5 Azul', 'prototype_bolt_m5_b.jpg', '1'),
(147, 41, 'Parafuso M5 Preto', 'Prototype_bolt_m5_bk_2.jpg', '2'),
(148, 41, 'Parafuso M5 Azul 2', 'prototype_bolt_m5_bl_2.jpg', '2'),
(149, 41, 'Parafuso M5 Dourado 2', 'prototype_bolt_m5_gd_2.jpg', '2'),
(150, 41, 'Parafuso M5 Dourado', 'prototype_bolt_m5_gd.jpg', '2'),
(151, 41, 'Parafuso M5 Verde', 'prototype_bolt_m5_gr.jpg', '2'),
(152, 41, 'Parafuso M5 Preto 2', 'prototype_bolt_m5_bk.jpg', '2'),
(153, 41, 'Parafuso M5 Verde 2', 'prototype_bolt_m5_gr_2.jpg', '2'),
(154, 41, 'Parafuso M5 Rosa', 'prototype_bolt_m5_pk.jpg', '2'),
(155, 41, 'Parafuso M5 Rosa 2', 'prototype_bolt_m5_pk_2.jpg', '2'),
(156, 41, 'Parafuso M5 Vermelho', 'prototype_bolt_m5_red.jpg', '2'),
(157, 41, 'Parafuso M5 Vermelho', 'prototype_bolt_m5_red2.jpg', '2'),
(158, 42, 'Logo Prototype', 'prototype-logo.png', '0'),
(159, 42, 'Parafuso M5 Azul', 'prototype_bolt_m6_b.jpg', '1'),
(160, 42, 'Parafuso M5 Preto', 'prototype_bolt_m6_bk.jpg', '2'),
(161, 42, 'Parafuso M5 Azul 2', 'prototype_bolt_m6_bll_2.jpg', '2'),
(162, 42, 'Parafuso M5 Dourado 2', 'prototype_bolt_m6_gd_2.jpg', '2'),
(163, 42, 'Parafuso M5 Dourado', 'prototype_bolt_m6_gd.jpg', '2'),
(164, 42, 'Parafuso M5 Verde', 'prototype_bolt_m6_gr.jpg', '2'),
(165, 42, 'Parafuso M5 Preto 2', 'prototype_bolt_m6_bk2.jpg', '2'),
(166, 42, 'Parafuso M5 Verde 2', 'prototype_bolt_m6_gr_2.jpg', '2'),
(167, 42, 'Parafuso M5 Rosa', 'prototype_bolt_m6_pk.jpg', '2'),
(168, 42, 'Parafuso M5 Rosa 2', 'prototype_bolt_m6_pk2.jpg', '2'),
(169, 42, 'Parafuso M5 Vermelho', 'prototype_bolt_m6_rd.jpg', '2'),
(170, 43, 'Logo Prototype', 'prototype-logo.png', '0'),
(171, 43, 'Parafuso Crack Preto', 'prototype_crb_bk.jpg', '1'),
(172, 43, 'Parafuso Crack Azul', 'prototype_crb_bl.jpg', '2'),
(173, 43, 'Parafuso Crack Dourado', 'prototype_crb_gd.jpg', '2'),
(174, 43, 'Parafuso Crack Verde', 'prototype_crb_gr.jpg', '2'),
(175, 43, 'Parafuso Crack Verde', 'prototype_crb_rd.jpg', '2'),
(176, 43, 'Parafuso Crack Dourado', 'prototype_crb_gd.jpg', '2'),
(177, 43, 'Parafuso Crack Dourado', 'prototype_crb_gd.jpg', '2'),
(178, 44, 'Logo Prototype', 'prototype-logo.png', '0'),
(179, 44, 'Prototype BS 1', 'prototype_bearing.jpg', '1'),
(180, 44, 'Prototype BS 2', 'prototype_bearing2.jpg', '2'),
(181, 44, 'Prototype BS 3', 'prototype_bearing3.jpg', '2'),
(182, 45, 'Logo Prototype', 'prototype-logo.png', '0'),
(183, 45, 'Parafuso Azul', 'prototype_bolt_bl.jpg', '1'),
(184, 45, 'Parafuso Rosa', 'prototype_bolt_pk.jpg', '2'),
(185, 45, 'Parafuso Vermelho', 'prototype_bolt_red.jpg', '2'),
(186, 45, 'Parafuso Vermelho', 'prototype_bolt_rotor_bk.jpg', '2'),
(187, 45, 'Parafuso Vermelho', 'prototype_bolt_rotor_gd.jpg', '2'),
(188, 45, 'Parafuso Vermelho', 'prototype_bolt_rotor_gr.jpg', '2'),
(189, 46, 'Logo Prototype', 'prototype-logo.png', '0'),
(190, 46, 'Speed Frente Preto', 'prototype_hub_speed_front_bk.jpg', '1'),
(191, 46, 'Speed Frente Branco', 'prototype_hub_speed_front_wh.jpg', '2'),
(192, 47, 'Logo Prototype', 'prototype-logo.png', '0'),
(193, 47, 'Speed Frente Preto', 'prototype_hub_speed_back_bk.jpg', '1'),
(194, 47, 'Speed Frente Branco', 'prototype_hub_speed_back_wh.jpg', '2'),
(195, 48, 'Logo Physis', 'physis.png', '0'),
(196, 48, 'Guiador Physis 1', 'prototype_handbar_01.jpg', '1'),
(197, 48, 'Guiador Physis 2', 'prototype_handbar_02.jpg', '2'),
(198, 48, 'Guiador Physis 3', 'prototype_handbar_03.jpg', '2'),
(199, 49, 'Logo Physis', 'physis.png', '0'),
(200, 49, 'Avanço Physis 6', 'prototype_physis_stem6.jpg', '1'),
(201, 49, 'Avanço Physis 17', 'prototype_physis_stem17.jpg', '2'),
(202, 50, 'Logo Prototype', 'prototype-logo.png', '0'),
(203, 50, 'CH3 150ml', 'ch3_150ml.jpg', '1'),
(204, 50, 'ch3 500ml', 'ch3_500ml.jpg', '2'),
(205, 50, 'ch3 5l', 'ch3_5l.jpg', '2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product-category_id` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `tiny_url` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tiny_url` (`tiny_url`),
  KEY `product-category_id` (`product-category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=51 ;

--
-- Extraindo dados da tabela `products`
--

INSERT INTO `products` (`id`, `product-category_id`, `name`, `description`, `tiny_url`) VALUES
(1, 4, '26R Race', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla gravida nibh. Nam dictum dolor et gravida placerat. Praesent eget consequat nulla, ut adipiscing risus. Morbi ornare sollicitudin purus nec interdum. Sed congue euismod hendrerit. In tellus urna, dignissim sit amet turpis vitae, porta vehicula risus. Donec est nulla, elementum in nulla a, sodales pulvinar tellus. Aliquam mattis a dolor eget luctus. Nullam mi mi, venenatis vel est sit amet, porta interdum felis. Nam vel neque nec tortor mollis tincidunt. Mauris bibendum at diam vel fermentum. Aliquam erat volutpat. Proin placerat risus eget facilisis hendrerit.', 'rim-mtb-26'),
(2, 4, '26R Carbon', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla gravida nibh. Nam dictum dolor et gravida placerat. Praesent eget consequat nulla, ut adipiscing risus. Morbi ornare sollicitudin purus nec interdum. Sed congue euismod hendrerit. In tellus urna, dignissim sit amet turpis vitae, porta vehicula risus. Donec est nulla, elementum in nulla a, sodales pulvinar tellus. Aliquam mattis a dolor eget luctus. Nullam mi mi, venenatis vel est sit amet, porta interdum felis. Nam vel neque nec tortor mollis tincidunt. Mauris bibendum at diam vel fermentum. Aliquam erat volutpat. Proin placerat risus eget facilisis hendrerit.', 'rim-mtb-26-carbon'),
(3, 4, '650B', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla gravida nibh. Nam dictum dolor et gravida placerat. Praesent eget consequat nulla, ut adipiscing risus. Morbi ornare sollicitudin purus nec interdum. Sed congue euismod hendrerit. In tellus urna, dignissim sit amet turpis vitae, porta vehicula risus. Donec est nulla, elementum in nulla a, sodales pulvinar tellus. Aliquam mattis a dolor eget luctus. Nullam mi mi, venenatis vel est sit amet, porta interdum felis. Nam vel neque nec tortor mollis tincidunt. Mauris bibendum at diam vel fermentum. Aliquam erat volutpat. Proin placerat risus eget facilisis hendrerit.', 'rim-mtb-650b'),
(4, 4, '650B Carbon', 'O nosso objetivo para uma roda de competição em Cross Country é alcançar extrema rigidez com conforto, elevado poder de acelerações e redução das perdas aerodinâmicas/atrito. \nOrgulhamo-nos do trabalho que apresentamos na nossa gama de rodas de carbono.\n\nE porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".\n', 'rim-mtb-650b-carbon'),
(5, 4, '29R', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla gravida nibh. Nam dictum dolor et gravida placerat. Praesent eget consequat nulla, ut adipiscing risus. Morbi ornare sollicitudin purus nec interdum. Sed congue euismod hendrerit. In tellus urna, dignissim sit amet turpis vitae, porta vehicula risus. Donec est nulla, elementum in nulla a, sodales pulvinar tellus. Aliquam mattis a dolor eget luctus. Nullam mi mi, venenatis vel est sit amet, porta interdum felis. Nam vel neque nec tortor mollis tincidunt. Mauris bibendum at diam vel fermentum. Aliquam erat volutpat. Proin placerat risus eget facilisis hendrerit.', 'rim-mtb-29'),
(6, 4, '29R Carbon', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer fringilla gravida nibh. Nam dictum dolor et gravida placerat. Praesent eget consequat nulla, ut adipiscing risus. Morbi ornare sollicitudin purus nec interdum. Sed congue euismod hendrerit. In tellus urna, dignissim sit amet turpis vitae, porta vehicula risus. Donec est nulla, elementum in nulla a, sodales pulvinar tellus. Aliquam mattis a dolor eget luctus. Nullam mi mi, venenatis vel est sit amet, porta interdum felis. Nam vel neque nec tortor mollis tincidunt. Mauris bibendum at diam vel fermentum. Aliquam erat volutpat. Proin placerat risus eget facilisis hendrerit.', 'rim-mtb-29-carbon'),
(7, 6, 'Race', 'Perante os nossos clientes, assumimos o compromisso de lhes proporcionar prazer e satisfação ao disfrutarem de umas rodas leves, rígidas e com elevado poder de aceleração. <br/><br/>\nO fruto dos inúmeros testes realizados revela-se nos padrões de qualidade destas rodas,  assegurando a sua fiabilidade e facilidade de manutenção. <br/><br/>\nE porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".<br/>\n', 'wheel-mtb-race'),
(8, 6, 'Cross', 'Perante os nossos clientes, assumimos o compromisso de lhes proporcionar prazer e satisfação ao disfrutarem de umas rodas leves, rígidas e com elevado poder de aceleração. <br/><br/>\nO fruto dos inúmeros testes realizados revela-se nos padrões de qualidade destas rodas,  assegurando a sua fiabilidade e facilidade de manutenção. <br/><br/>\nE porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".<br/>\n', 'wheel-mtb-cross'),
(9, 6, 'Marathon', 'Perante os nossos clientes, assumimos o compromisso de lhes proporcionar prazer e satisfação ao disfrutarem de umas rodas leves, rígidas e com elevado poder de aceleração. <br/><br/>\nO fruto dos inúmeros testes realizados revela-se nos padrões de qualidade destas rodas,  assegurando a sua fiabilidade e facilidade de manutenção. <br/><br/>\nE porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".<br/>\n', 'wheel-mtb-marathon'),
(10, 6, '26R Carbon', 'O nosso objetivo para uma roda de competição em Cross Country é alcançar extrema rigidez com conforto, elevado poder de acelerações e redução das perdas aerodinâmicas/atrito. <br/><br/>\nOrgulhamo-nos do trabalho que apresentamos na nossa gama de rodas de carbono. <br/><br/>\n\nE porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".\n', 'wheel-mtb-26-carbon'),
(11, 6, '650B', 'Perante os nossos clientes, assumimos o compromisso de lhes proporcionar prazer e satisfação ao disfrutarem de umas rodas leves, rígidas e com elevado poder de aceleração. <br/><br/>\nO fruto dos inúmeros testes realizados revela-se nos padrões de qualidade destas rodas,  assegurando a sua fiabilidade e facilidade de manutenção. <br/><br/>\nE porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".<br/>\n', 'wheel-mtb-650b'),
(12, 6, '650B Carbon', 'O nosso objetivo para uma roda de competição em Cross Country é alcançar extrema rigidez com conforto, elevado poder de acelerações e redução das perdas aerodinâmicas/atrito. <br/><br/>\nOrgulhamo-nos do trabalho que apresentamos na nossa gama de rodas de carbono. <br/><br/>\n\nE porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".\n', 'wheel-mtb-650b-carbon'),
(13, 6, '29R', 'Perante os nossos clientes, assumimos o compromisso de lhes proporcionar prazer e satisfação ao disfrutarem de umas rodas leves, rígidas e com elevado poder de aceleração. <br/><br/>\nO fruto dos inúmeros testes realizados revela-se nos padrões de qualidade destas rodas,  assegurando a sua fiabilidade e facilidade de manutenção. <br/><br/>\nE porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".<br/>\n', 'wheel-mtb-29'),
(14, 6, '29R Carbon', 'O nosso objetivo para uma roda de competição em Cross Country é alcançar extrema rigidez com conforto, elevado poder de acelerações e redução das perdas aerodinâmicas/atrito. <br/><br/>\nOrgulhamo-nos do trabalho que apresentamos na nossa gama de rodas de carbono. <br/><br/>\n\nE porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".\n', 'wheel-mtb-29-carbon'),
(15, 7, 'Espigão Physis', 'O espigão de selim Physis é muito leve, confortável e resistente. O sistema de fixação do selim foi desenvolvido e reforçado de modo a permitir um perfeito ajuste mesmo em quadros onde o ângulo de inclinação do espigão é elevado.', 'physis-seatpost'),
(16, 10, 'Evo3 Frente', 'Desfrute da suavidade que o nosso cubo EVO3 proporcionam', 'evo3-fw'),
(17, 10, 'Evo3 Trás', 'Desfrute da suavidade que o nosso cubo EVO3 proporcionam', 'evo3-rw'),
(18, 10, 'Evo3 Lefty', 'Desfrute da suavidade que o nosso cubo EVO3 proporcionam', 'evo3-lefty'),
(20, 12, 'Physis - Extensor de Guiador', 'O excelente compromisso entre os 9,5º de convergência e a ergonomia da pega conferem ao extensor de guiador Physis as características ideais para os utilizadores mais exigentes.', 'physis-barend'),
(21, 13, 'Válvulas Bronze', 'Válvula tubeless universal de interior removível com base retangular em borracha para uma perfeita vedação..  ', 'bronze-valve'),
(22, 13, 'Fita UST', 'Fita de aro PROTOTYPE para conversão tubeless. Modo de utilização muito simples e compatível com os aros Prototype, No Tubes, DT Swiss, FRM e Mavic.', 'ust-tape'),
(23, 14, 'Aperto Cassete', 'Fecho de cassete colorido e extremamente forte. Com apenas 5.8 gr este aperto é compatível com cassetes Shimano e Sram cujo o último andamento tenha 11 dentes.', 'lockring'),
(24, 16, 'Bidão 600ml', 'Bidão de 600 ml com tampa de rosca.\r\nO bocal largo permite facilmente introduzir cubos de gelo e produtos diatéticos. A lavagem também se torna simples e pode ser colocado na máquina da louça.\r\nO plástico não liberta sabores nem deita cheiros. \r\n', 'bottle'),
(25, 17, 'Roldanas', 'O design aberto e perfilado combina a baixa resistência aerodinâmica com a maior capacidade de limpeza e baixa acumulação de resíduos.  A utilização de rolamentos selado de baixa fricção assegura uma redução de percas por atrito sem comprometer a durabilidade. \nO corpo é  maquinado em CNC e a espessura dos dentes estudada para uma perfeita e rápida passagem de mudança. \n', 'jockey-wheels'),
(26, 13, 'Válvulas Alumínio', 'Válvula tubeless universal de interior removível com base cónica em borracha para uma perfeita vedação. Adapta-se muito bem ao aro, mesmo quando o furo é maior.', 'aluminium-valve'),
(27, 5, '3 Pro Tour Clincher', 'Vínculo absoluto entre uma roda de excelência e um baixo preço. Pro Tour é uma gama versátil para ser utilizada em competição ou lazer. Aspetos como rigidez, reduzido atrito, fiabilidade, estética e baixo peso, são os pontos fortes desta gama. Disponível nas versões pneu e tubular. O perfil de 38mm é recomendado para todo o tipo de utilização. Peso limite do atleta 95 kg. E porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".', '3-pro-tour-c'),
(28, 5, '3 Pro Tour Tubular', 'Vínculo absoluto entre uma roda de excelência e um baixo preço. Pro Tour é uma gama versátil para ser utilizada em competição ou lazer. Aspetos como rigidez, reduzido atrito, fiabilidade, estética e baixo peso, são os pontos fortes desta gama. Disponível nas versões pneu e tubular. O perfil de 38mm é recomendado para todo o tipo de utilização. Peso limite do atleta 95 kg. E porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".', '3-pro-tour-t'),
(29, 5, '5 Pro Tour Clincher', 'Vínculo absoluto entre uma roda de excelência e um baixo preço. Pro Tour é uma gama versátil para ser utilizada em competição ou lazer. Aspetos como rigidez, reduzido atrito, fiabilidade, estética e baixo peso, são os pontos fortes desta gama. Disponível nas versões pneu e tubular. O perfil de 50mm é recomendado para todo o tipo de utilização. Peso limite do atleta 110 kg. E porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".', '5-pro-tour-c'),
(30, 5, '5 Pro Tour Tubular', 'Vínculo absoluto entre uma roda de excelência e um baixo preço. Pro Tour é uma gama versátil para ser utilizada em competição ou lazer. Aspetos como rigidez, reduzido atrito, fiabilidade, estética e baixo peso, são os pontos fortes desta gama. Disponível nas versões pneu e tubular. O perfil de 50mm é recomendado para todo o tipo de utilização. Peso limite do atleta 110 kg. E porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".', '5-pro-tour-t'),
(31, 5, '2 Pro Tour SL', 'Perfeito compromisso entre máxima rigidez, baixo peso e aerodinâmica. Pro Tour SL é uma gama de rodas desenvolvida para competição capaz de satisfazer os atletas mais experientes. Equipada com os rolamentos  especiais de alta precisão, os brilhantes resultados obtidos em provas de classe mundial comprovam a sua elevada fiabilidade. Orgulhamo-nos do trabalho que apresentamos na nossa gama de rodas de carbono. Incrivelmente leves e com perfil de 20mm são recomendas para enfrentar as montanhas mais duras e surpreender os seus adversários. São também a delícia dos amantes de componentes light weight. E porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".', '2-pro-tour-sl'),
(32, 5, '3 Pro Tour SL', 'Perfeito compromisso entre máxima rigidez, baixo peso e aerodinâmica. Pro Tour SL é uma gama de rodas desenvolvida para competição capaz de satisfazer os atletas mais experientes. Equipada com os rolamentos  especiais de alta precisão, os brilhantes resultados obtidos em provas de classe mundial comprovam a sua elevada fiabilidade. Orgulhamo-nos do trabalho que apresentamos na nossa gama de rodas de carbono. O perfil de 38mm é recomendado para todo o tipo de provas com vento lateral. Peso limite do atleta 110 kg. E porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".', '3-pro-tour-sl'),
(33, 5, '5 Pro Tour Tubular', 'Perfeito compromisso entre máxima rigidez, baixo peso e aerodinâmica. Pro Tour SL é uma gama de rodas desenvolvida para competição capaz de satisfazer os atletas mais experientes. Equipada com os rolamentos  especiais de alta precisão, os brilhantes resultados obtidos em provas de classe mundial comprovam a sua elevada fiabilidade. Orgulhamo-nos do trabalho que apresentamos na nossa gama de rodas de carbono. O perfil de 50mm é recomendado para todo o tipo de provas com pouco vento lateral. Peso limite do atleta 110 kg. E porque os azares acontecem, consulte os nossos planos de assistência e manutenção "Technical Assistance" e "Crash Replacement".', '5-pro-tour-sl'),
(34, 15, 'Anilha Direção', 'Espaçadores para a coluna de direção em alumínio Light Force super ligeiros. Permite reduzir o peso até 70%! Conjunto composto por dois espaçadores: 1x   5 mm + 1x 10 mm.', 'spacer-headset'),
(35, 15, 'Anilha Movimento Pedaleiro', 'Espaçadores coloridos para movimentos pedaleiros em alumínio.Aumenta a rigidez do bloco pedaleiro e reduz ruídos originados pelas convencionais anilhas em plástico. Compatíveis com sistema BSA, GXP, Italianos.', 'spacer-bottombracket'),
(36, 15, 'Anilha Prato Pedaleiro', 'Anilhas de afinação para ajustar o offset dos pratos pedaleiros ou a linha de corrente. Assim, torna o sistema de transmissão mais duradouro quando utiliza andamentos cruzados e previne a queda da corrente entre os pratos. ', 'spacer-crank'),
(37, 18, 'Aperto Espigão', 'Maquinado a CNC de um bloco maciço de Alumínio, resulta num elegante aperto com o requinte dado por suas linhas suaves e simples. O modo consistente como abraça o quadro, combinado com um parafuso de Titânio, permite um aperto efetivo do espigão mais firme e uniforme.', 'seat-clamp'),
(38, 19, 'Caixa Carbono', 'A montagem da caixa interior da mudança em carbono confere uma maior rigidez e uma redução de peso. As suas vantagens traduzem-se num aumento de precisão, rapidez na passagem das mudanças e num maior tensionamento da corrente.Especialmente recomendada para os aficionados do peso, a gama SL é extremamente leve e contra indicada para a utilização em competição.', 'carbon-cage'),
(39, 20, 'BSA', 'Movimento pedaleiro PROTOTYPE produzido a partir de um bloco maciço e maquinado a CNC. Este movimento é compatível com sistema Shimano BSA. Equipado com rolamentos de baixa fricção e substituíveis PROTOTYPE Black Shield.', 'bb-bsa'),
(40, 20, 'BSA', 'Movimento pedaleiro PROTOTYPE produzido a partir de um bloco maciço e maquinado a CNC. Este movimento é compatível com sistema SRAM e TRUVATIV GXP. Equipado com rolamentos de baixa fricção e substituíveis PROTOTYPE Black Shield.', 'bb-gxp'),
(41, 21, 'Parafusos M5', NULL, 'bolt-m5'),
(42, 21, 'Parafusos M6', NULL, 'bolt-m6'),
(43, 21, 'Parafusos Kit Pedaleiro', NULL, 'bolt-crack-kit'),
(44, 22, 'Rolamentos Black Shield', 'Rolamentos selados de alta precisão (Abec5) com endurecimento de superfície e tratamento anti-corrosão.', 'bearing-black-shield'),
(45, 21, 'Parafusos Disco', NULL, 'disc-bolt'),
(46, 9, 'Speed Frente', NULL, 'speed-fw'),
(47, 9, 'Speed Trás', NULL, 'speed-bw'),
(48, 23, 'Guiador Physis', 'A capacidade de absorção das vibrações é fundamental para reduzir a fadiga enquanto a rigidez é essencial para um prefeito dominio da bicicleta. O guiador Physis é rigido, reforçado nas zonas de aperto e muito leve. Consideramos que a ergonomia, conforto e segurança são elemtnos primordiais para um guiador, pelo que dispomos de dois comprimentos para alcançar a necessidades de todos os atletas.', 'physis-handlebar'),
(49, 24, 'Avanço Physis', 'Consientes da importância do avanço como elemento imperativo para o perfeito ajuste da posição do atleta, o modelo Physis distingue-se dos demais pelo seu design, materiais de construção e capacidade de absorção de vibrações', 'physis-stem'),
(50, 12, 'CH3', 'O líquido CH3 é um selante desenvolvido pela PROTOTYPE para utilização em pneus tubeless e câmaras-de-ar para utilização de bicicleta. Devido à sua composição química, CH3 é um produto de atuação instantânea e de fácil limpeza.<br>\nO líquido CH3 é um selante desenvolvido pela PROTOTYPE para utilização em pneus tubeless e câmaras-de-ar para utilização de bicicleta. Possui uma capacidade de vedação e reparação de furos praticamente instantânea em perfurações até 6mm. Para um perfeito funcionamento, recomendamos a verificação a cada 3 meses da quantidade existente e, se necessário, a\nadição de líquido.<br>\nDisponível em embalagens de:<br> \n75 ml<br>\n150 ml<br>\n500 ml<br>\n5L', 'ch3');

-- --------------------------------------------------------

--
-- Estrutura da tabela `products-category`
--

CREATE TABLE IF NOT EXISTS `products-category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product-category_id` int(11) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product-category_id` (`product-category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Extraindo dados da tabela `products-category`
--

INSERT INTO `products-category` (`id`, `product-category_id`, `name`) VALUES
(0, 0, 'no-parent'),
(1, 0, 'Rim'),
(2, 0, 'Wheel'),
(3, 1, 'Road'),
(4, 1, 'MTB'),
(5, 2, 'Road'),
(6, 2, 'MTB'),
(7, 0, 'Espigão de Selim'),
(8, 0, 'Cubos'),
(9, 8, 'Estrada'),
(10, 8, 'Montanha'),
(11, 8, 'Acessórios'),
(12, 0, 'Extensor Guiador'),
(13, 0, 'Acessórios Tubeless'),
(14, 0, 'Aperto Cassete'),
(15, 0, 'Anilhas'),
(16, 0, 'Bidão'),
(17, 0, 'Roldanas'),
(18, 0, 'Aperto Espigão'),
(19, 0, 'Caixas de Carbono'),
(20, 0, 'Movimento Pedaleiro'),
(21, 0, 'Parafusos AL'),
(22, 0, 'Rolamentos'),
(23, 0, 'Guiador'),
(24, 0, 'Avanço');

-- --------------------------------------------------------

--
-- Estrutura da tabela `registers`
--

CREATE TABLE IF NOT EXISTS `registers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nif` int(11) NOT NULL,
  `product` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `buy_date` date NOT NULL,
  `shop_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `registers`
--

INSERT INTO `registers` (`id`, `name`, `nif`, `product`, `serial_number`, `email`, `buy_date`, `shop_name`, `created_at`, `updated_at`) VALUES
(3, '', 0, '', '', '', '0000-00-00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Carlos Morgado', 242654509, 'Roda Frente Lefty', '13A6501682', 'c428morgado@hotmail.com', '0000-00-00', 'Fatibike', '2013-10-22 20:03:13', '2013-10-22 20:03:13'),
(5, 'Carlos Morgado', 242654509, 'Roda Trás', '13A6501697', 'c428morgado@hotmail.com', '0000-00-00', 'Fatibike', '2013-10-22 20:03:49', '2013-10-22 20:03:49'),
(6, 'NELSON MARQUES', 208367403, 'RODAS CARBONO 29', '13CW23', 'NELSONGFM@GMAIL.COM', '0000-00-00', 'BIKE ZONE BARCELOS', '2014-02-02 17:02:08', '2014-02-02 17:02:08'),
(7, 'NELSON MARQUES', 208367403, 'RODAS CARBONO 29', '13CW32', 'NELSONGFM@GMAIL.COM', '0000-00-00', 'BIKE ZONE BARCELOS', '2014-02-02 17:03:01', '2014-02-02 17:03:01'),
(8, 'Miguel Coelho', 232134650, 'Roda 29R', '14A290A2449', 'mamscoelho@gmail.com', '0000-00-00', 'MP1Bikes (Francisco Ceia Calha', '2014-03-19 17:47:00', '2014-03-19 17:47:00'),
(9, 'Miguel Coelho', 232134650, 'Roda 29R', '14B29A10019', 'mamscoelho@gmail.com', '0000-00-00', 'MP1Bikes (Francisco Ceia Calha', '2014-03-19 17:48:05', '2014-03-19 17:48:05'),
(10, 'Flavio Vicente', 200384201, 'rodas 29er', '14CW05RM03036', 'flaviomlvicente@hotmail.com', '0000-00-00', 'Fatibike', '2014-03-22 10:55:03', '2014-03-22 10:55:03');

-- --------------------------------------------------------

--
-- Estrutura da tabela `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2013-08-12 16:13:03', '2013-08-12 16:13:03'),
(2, 'comment', '2013-08-12 16:13:03', '2013-08-12 16:13:03');

-- --------------------------------------------------------

--
-- Estrutura da tabela `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `slides`
--

INSERT INTO `slides` (`id`, `img`, `link`, `created_at`, `updated_at`) VALUES
(1, 'img/slides/banner_prototype_xx1.jpg', '#', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'img/slides/banner_prototype_physis.jpg', '#', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'img/slides/banner_prototype_ch3.jpg', '#', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'img/slides/banner_prototype_gxp.jpg', '#', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `spec-product`
--

CREATE TABLE IF NOT EXISTS `spec-product` (
  `product_id` int(11) unsigned NOT NULL,
  `spec_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`spec_id`),
  KEY `spec_id` (`spec_id`),
  KEY `spec_id_2` (`spec_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `spec-product`
--

INSERT INTO `spec-product` (`product_id`, `spec_id`) VALUES
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(39, 1),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(16, 3),
(17, 3),
(18, 3),
(20, 3),
(21, 3),
(23, 3),
(26, 3),
(34, 3),
(35, 3),
(36, 3),
(37, 3),
(38, 3),
(39, 3),
(40, 3),
(42, 3),
(44, 3),
(45, 3),
(39, 7),
(40, 7),
(7, 8),
(8, 8),
(9, 8),
(10, 8),
(11, 8),
(12, 8),
(13, 8),
(14, 8),
(7, 9),
(8, 9),
(9, 9),
(10, 9),
(11, 9),
(12, 9),
(13, 9),
(14, 9),
(7, 10),
(8, 10),
(9, 10),
(10, 10),
(11, 10),
(12, 10),
(13, 10),
(14, 10),
(7, 11),
(8, 11),
(9, 11),
(10, 11),
(11, 11),
(12, 11),
(13, 11),
(14, 11),
(10, 12),
(12, 12),
(14, 12),
(7, 13),
(8, 13),
(9, 13),
(10, 13),
(11, 13),
(12, 13),
(13, 13),
(14, 13),
(7, 14),
(8, 14),
(9, 14),
(10, 14),
(11, 14),
(12, 14),
(13, 14),
(14, 14),
(7, 15),
(8, 15),
(9, 15),
(10, 15),
(11, 15),
(12, 15),
(13, 15),
(14, 15),
(7, 16),
(8, 16),
(9, 16),
(10, 16),
(11, 16),
(12, 16),
(13, 16),
(14, 16),
(7, 17),
(8, 17),
(9, 17),
(10, 17),
(11, 17),
(12, 17),
(13, 17),
(14, 17),
(7, 18),
(8, 18),
(9, 18),
(10, 18),
(11, 18),
(12, 18),
(13, 18),
(14, 18),
(7, 20),
(8, 20),
(9, 20),
(11, 20),
(13, 20),
(7, 22),
(8, 22),
(9, 22),
(11, 22),
(13, 22);

-- --------------------------------------------------------

--
-- Estrutura da tabela `specs`
--

CREATE TABLE IF NOT EXISTS `specs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Extraindo dados da tabela `specs`
--

INSERT INTO `specs` (`id`, `name`, `about`, `img`, `type`) VALUES
(1, 'Easy Mount', 'Fácil Manutenção / Substituição', 'easy-mount.png', '2'),
(3, 'Light Force', 'A PROTOTYPE escolhe para os seus componentes ligas de alumínio das séries 2000, 6000 e 7000. A escolha do material e dos tratamentos dados ao alumínio depende das solicitações mecânicas a que cada elemento está sujeito. Light Force é nosso alumínio depois de ser manipulado.', 'light-force.png', '0'),
(7, 'Black Shield', 'Rolamentos selados de alta precisão (Abec5) com endurecimento de supreficie e  tratamento anti-corrosão.', 'black_shield-logo.png', '0'),
(8, '12 Low Friction', 'Sistema de engrenagem de baixa fricção com uma amplitude angular de apenas 12 graus entre pontos.', '12_low_friction-logo.png', '0'),
(9, 'N-Force', 'Reforço estrutural nas cabeças de raio.', 'n_force-logo.png', '0'),
(10, 'Hand Made', 'Montagem e controlo de qualidade manual.', 'hand_made-logo.png', '0'),
(11, 'Lefty', 'Montagem disponível com cubo Lefty.', 'evo3_lefty-logo.png', '1'),
(12, 'Raio Aero SL', 'Montagem disponível com raios ultra ligeiros (Pillar X-TRA)', 'aero_spoke_sl-logo.png', '1'),
(13, '15 x 100', 'Compatibilidade com sistemas 15 x 100mm. Conversores adquiridos em separado.', 'comp_15_100-logo.png', '1'),
(14, '12 x 142', 'Compatibilidade com sistemas 12 x 142mm. Disponível para Shimano/Sram ou Sram XX1 / XO1. Conversores adquiridos em separado.', 'comp_12_142-logo.png', '1'),
(15, '12 x 135', 'Compatibilidade com sistemas 15 x 135mm. Conversores adquiridos em separado.', 'comp_12_135-logo.png', '1'),
(16, 'XO1-XX1', 'Montagem disponível com sistema Sram XX1, XO1.', 'comp_X01_XX1-logo.png', '1'),
(17, 'Tubeless-Ready', 'Preparado para converter em Tubeless.', 'tubeless_ready-logo.png', '2'),
(18, 'Rolamentos Selados', 'Equipado rolamentos selados', 'sealed_bearing-logo.png', '2'),
(19, 'Rolamentos Cerâmicos', 'Equipado rolamentos cerâmicos', 'ceramic_bearing-logo.png', '2'),
(20, 'Aro Reforçado', 'Aro reforçado na soldadura', 'reinforced_rim-logo.png', '2'),
(21, 'Raio Aero', 'Raio aerodinâmico', 'aero_spoke-logo.png', '2'),
(22, 'Raio Forjado', 'Raio reforçado e ultra resistente', 'forged_spoke-logo.png', '2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `support-category`
--

CREATE TABLE IF NOT EXISTS `support-category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `support-category`
--

INSERT INTO `support-category` (`id`, `name`) VALUES
(1, 'Manuais'),
(2, 'Instalação');

-- --------------------------------------------------------

--
-- Estrutura da tabela `support-product`
--

CREATE TABLE IF NOT EXISTS `support-product` (
  `product_id` int(10) unsigned NOT NULL,
  `support_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`support_id`),
  KEY `product_id` (`product_id`),
  KEY `support_id` (`support_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `support-product`
--

INSERT INTO `support-product` (`product_id`, `support_id`) VALUES
(15, 9),
(27, 3),
(28, 3),
(29, 3),
(30, 3),
(31, 3),
(32, 3),
(37, 10),
(49, 6),
(49, 7),
(49, 8);

-- --------------------------------------------------------

--
-- Estrutura da tabela `supports`
--

CREATE TABLE IF NOT EXISTS `supports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idCategory` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `lang` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `supports`
--

INSERT INTO `supports` (`id`, `idCategory`, `name`, `path`, `lang`) VALUES
(3, 1, 'Manual Rodas Estrada - PT', 'manual_prototype_road.pdf', 'pt'),
(4, 1, 'Catalogo Prototype PT', 'prototype_catalogo_pt.pdf', 'pt'),
(5, 1, 'Catalogo Prototype - ES', 'prototype_catalogo_es.pdf', 'es'),
(6, 2, 'Manual de Instruções Avanço', 'physis_stem_install_pt_en_es.pdf', 'pt'),
(7, 2, 'Manual de Instrucciones Potencia', 'physis_stem_install_pt_en_es.pdf', 'es'),
(8, 2, 'Instruction Manual Stem', 'physis_stem_install_pt_en_es.pdf', 'en'),
(9, 2, 'Manual de Instruções Espigão de Selim', 'physis_sp_install_pt.pdf', 'pt'),
(10, 2, 'Manual de Instruções Aperto de Espigão de Selim', 'physis_sc_install_pt.pdf', 'pt');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmation_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `confirmation_code`, `confirmed`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@example.com', '$2y$08$ZoGp1oCjdQ4aexfU8nI9N.SJJAUuSJtAWNPhE0Kj7VyAfZD2dZS4u', '778418c2a4326ac56f6806f7ea03531c', 1, '2013-08-12 16:13:02', '2013-08-12 16:13:02'),
(2, 'user', 'user@example.org', '$2y$08$355oF81an6rV2FtChb.Z9uhswJiEKzeFGa58CIMxac.ov0PKt7gbK', 'a2413f8b0a12f5459543e63ebf77fea3', 1, '2013-08-12 16:13:03', '2013-08-12 16:13:03'),
(3, 'jonioliveira', 'jo.racingdesign@gmail.com', '$2y$08$VQkkLbJhX86b7q4WLQNWBOQGGUolzSVOgF6CjzoIScDFkOz/FpfWe', 'ff5c1a05bfb654d47b269f45b91278a1', 0, '2013-11-13 23:02:48', '2013-11-13 23:02:48');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `assigned_roles`
--
ALTER TABLE `assigned_roles`
  ADD CONSTRAINT `assigned_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `assigned_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`);

--
-- Limitadores para a tabela `features`
--
ALTER TABLE `features`
  ADD CONSTRAINT `features_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Limitadores para a tabela `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Limitadores para a tabela `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`posts-category_id`) REFERENCES `posts-category` (`id`),
  ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `product-images`
--
ALTER TABLE `product-images`
  ADD CONSTRAINT `product@002dimages_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Limitadores para a tabela `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`product-category_id`) REFERENCES `products-category` (`id`);

--
-- Limitadores para a tabela `products-category`
--
ALTER TABLE `products-category`
  ADD CONSTRAINT `products@002dcategory_ibfk_1` FOREIGN KEY (`product-category_id`) REFERENCES `products-category` (`id`);

--
-- Limitadores para a tabela `spec-product`
--
ALTER TABLE `spec-product`
  ADD CONSTRAINT `spec-product_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Limitadores para a tabela `support-product`
--
ALTER TABLE `support-product`
  ADD CONSTRAINT `support-product_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `support-product_ibfk_2` FOREIGN KEY (`support_id`) REFERENCES `supports` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
